package com.sstech.janusgraph.diskstorage.snowflake;

import com.sstech.janusgraph.common.storage.SnowFlakeDBStorageSetup;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.KeyValueStoreTest;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.OrderedKeyValueStoreManager;


public class SnowFlakeKeyValueTest extends KeyValueStoreTest {
    @Override
    public OrderedKeyValueStoreManager openStorageManager() throws BackendException {
        return new SnowFlakeStoreManager(SnowFlakeDBStorageSetup.getSnowFlakeDBConfiguration());
    }
}
