// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package com.sstech.janusgraph.snowflake.snowflakedb;

// This is the helper class to provide a easy to use helper reference for doing SnowFlake based functionalities
// graph = JanusGraphFactory.open("conf/janusgraph-snowflake.properties")

import com.google.common.base.Preconditions;
import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.common.utils.SingleFileLogger;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import com.sstech.janusgraph.snowflake.utils.OrderSnowFlakeDB;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;

import java.io.IOException;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class SnowFlakeDatabase implements Serializable {
    private String tableName;
    private SnowFlakeTransaction transaction;
    private SnowFlakeConnection connection;
    private boolean KEYEXIST = false;

    public boolean tableOrdered = false;

    private Integer insertBatchSize;

//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeDatabase.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
    private static SingleFileLogger logger = SingleFileLogger.getInstance();
    private Logger log = logger.getLogger();

    public  SnowFlakeDatabase(String name, Integer INSERT_BATCH_SIZE, SnowFlakeTransaction tx) throws IOException, SnowFlakeConnectionException {

        transaction = tx;
        tableName = name;
        connection = transaction.getConnection();
        insertBatchSize = INSERT_BATCH_SIZE;

        if (!tableName.equals("")) {
            getOrCreateTable(tableName);
        }
    }

    public SnowFlakeDatabase(String name, SnowFlakeTransaction tx) throws IOException, SnowFlakeConnectionException {
        this(name, 10000, tx);
    }

    public SnowFlakeDatabase(String name, SnowFlakeConnection connection) throws IOException {
        // TODO : Need to add GET OR CREATE TABLE SnowFlake statement & execute the query
        tableName = name;
        this.connection = connection;

        if (!tableName.equals("")) {
            getOrCreateTable(tableName);
        }
    }

    private Boolean getOrCreateTable(String tableName) {
//        String sql ="CREATE OR REPLACE SEQUENCE seq1;";
//        connection.executeUpdate(sql);
        String sql = "CREATE TABLE " + tableName + " IF NOT EXISTS (KEY BINARY, VALUE BINARY, COLUMN1 BINARY);";
        OperationStatus res = connection.executeUpdate(sql);
        return res.getStatus();
    }

    private byte[] getByteArray(String encodedString) {
        return Base64.getDecoder().decode(encodedString);
    }

    private StaticBuffer getBuffer(byte[] entry) {
        return new StaticArrayBuffer(entry);
    }

    public SnowFlakeConnection getConnection() {
        return connection;
    }

    private void orderTable() {
        boolean status;
        try {
            new OrderSnowFlakeDB(tableName, connection).order();
            status = true;
        } catch (SQLException e) {
            e.printStackTrace();
            status = false;
        }
        if (status)
            System.out.println("Ordering of table " + tableName + " complete");
        else
            System.out.println("Ordering of table " + tableName + " couldn't be processed");
    }

    private String generateBulkLoadQueryValue(ArrayList<byte[]> keyList, ArrayList<byte[]> valueList) {
        String insertStatement = "";

        for (int i = 0; i < keyList.size(); i++) {
            byte[] key = keyList.get(i);
            byte[] value = valueList.get(i);

            String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
            String valueBase64Encoded = Base64.getEncoder().encodeToString(value);

            String subSQL;
            if (i < keyList.size() - 1)
                subSQL = "(to_binary('" + keyBase64Encoded + "', 'BASE64'), to_binary('" + valueBase64Encoded + "', 'BASE64')),";
            else
                subSQL = "(to_binary('" + keyBase64Encoded + "', 'BASE64'), to_binary('" + valueBase64Encoded + "', 'BASE64'));";

            insertStatement += subSQL;
        }
        return insertStatement;
    }

    public List<ArrayList<byte[]>> splitBigByteArray(ArrayList<byte[]> bigArray) {
        List<ArrayList<byte[]>> partitions = new LinkedList<>();
        for (int i = 0; i < bigArray.size(); i += insertBatchSize) {
            List<byte[]> subList = bigArray.subList(i, Math.min(i+insertBatchSize, bigArray.size()));
            partitions.add(new ArrayList<>(subList));
        }
        return partitions;
    }

    public OperationStatus bulkPut(ArrayList<byte[]> keyList, ArrayList<byte[]> valueList) {
        if (keyList.size() > 0) {

            if (insertBatchSize > keyList.size()) {
                String insertStatement = "INSERT INTO " + tableName
                    + " (KEY, VALUE) VALUES ";

                String subSQL = generateBulkLoadQueryValue(keyList, valueList);

                insertStatement = insertStatement + subSQL;

                return connection.executeUpdate(insertStatement);
            }
            else {
                List<ArrayList<byte[]>> keyPartitions = splitBigByteArray(keyList);
                List<ArrayList<byte[]>> valuePartitions = splitBigByteArray(valueList);

                // The sizes should be equal
                Preconditions.checkArgument(keyPartitions.size() == valuePartitions.size());

                OperationStatus result = new OperationStatus(true);

                for (int i = 0; i < keyPartitions.size(); i++) {
                    // Create a Fresh Insert Statement. One Insert statement for single batch of insert
                    String insertStatement = "INSERT INTO " + tableName
                        + " (KEY, VALUE) VALUES ";

                    ArrayList<byte[]> keyPartition = keyPartitions.get(i);
                    ArrayList<byte[]> valuePartition = valuePartitions.get(i);

                    String subSQL = generateBulkLoadQueryValue(keyPartition, valuePartition);

                    insertStatement = insertStatement + subSQL;
                    OperationStatus res = connection.executeUpdate(insertStatement);

                    if (!res.getStatus()) {
                        // If any of batch fails, we assign the status of failure to default result
                        result.setOperationStatus(false);
                    }
                }
                return result;
            }
        }
        else {
            return null;
        }
    }

    public OperationStatus put(StaticBuffer key, StaticBuffer value) {
        // This currently is generic put, without checking if entry exists in DB. Need to implement
        // putNoOverWrite which shall insert only when data is absent else throw error

        String statement = "INSERT INTO " + tableName + " (KEY, VALUE) VALUES " + key + value;

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
                "put", tableName, key.toString(), value.toString(), statement));

        OperationStatus result = connection.executeUpdate(statement);

        // Now that INSERT operation is done, we need to order the data
        if (tableOrdered)
            orderTable();

        return result;
    }

    public OperationStatus put(byte[] key, byte[] value) throws SQLException {
        // TODO - DONE
        // This currently is generic put, without checking if entry exists in DB. Need to implement
        // putNoOverWrite which shall insert only when data is absent else throw error
        // TODO
        // TODO: While inserting preserve the order of keys. Note that the backend needs to be ordered Key Value store.
        // TODO: So, whenever inserting, first query for appropriate ID, then insert at that place and push others down.
        // TODO: We created a OrderSnowFlakeDB class which creates a temp table, inserts in ordered way and then renamed
        // TODO: new table. We need to call this everything a insert happens, and check for performance bottlenecks.

        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
        String valueBase64Encoded = Base64.getEncoder().encodeToString(value);

        String countQuery = LogUtils.stringFormatter("select count(*) from {} where KEY = to_binary('{}', " +
            "'BASE64')", tableName, keyBase64Encoded);
        OperationStatus res = connection.executeQuery(countQuery);
        int rows = 0;
        try {
            rows = res.getResultSize(1);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        }

//        System.out.println("Number of entries for key=" + getBuffer(key).getLong(0) + " is " + rows);
//        System.out.println(countQuery);
        if (rows == 0) {
            String insertStatement = "INSERT INTO " + tableName
                + " (KEY, VALUE) VALUES (to_binary('" + keyBase64Encoded + "', 'BASE64'), " +
                "to_binary('" + valueBase64Encoded + "', 'BASE64'))";

            log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
                "put(encoded)", tableName, keyBase64Encoded, valueBase64Encoded, insertStatement));

            OperationStatus result = connection.executeUpdate(insertStatement);

            connection.executeUpdate("commit");

            // Now that INSERT operation is done, we need to order the data
            if (tableOrdered)
                orderTable();

            return result;
        }
        else {
            String updateStatement = "UPDATE " + tableName
                + " SET KEY = to_binary('" + keyBase64Encoded + "', 'BASE64'), " +
                "VALUE = to_binary('" + valueBase64Encoded + "', 'BASE64') " +
            "WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

            log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
                "put(encoded)", tableName, keyBase64Encoded, valueBase64Encoded, updateStatement));

            OperationStatus result = connection.executeUpdate(updateStatement);

            connection.executeUpdate("commit");

            // Now that INSERT operation is done, we need to order the data
            if (tableOrdered)
                orderTable();

            return result;
        }
    }

    public OperationStatus putNotOverWrite(StaticBuffer key, StaticBuffer value) {
        String statement = "SELECT COUNT(*) FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64')";
        if ((Integer) connection.executeQuery(statement).getResult() > 0) {
            String insertStatement = "INSERT INTO " + tableName + " (KEY, VALUE) VALUES " + key + value;

            OperationStatus result = connection.executeUpdate(insertStatement);

            // Now that INSERT operation is done, we need to order the data
            if (tableOrdered)
                orderTable();

            return result;
        }
        else {
            OperationStatus status = new OperationStatus(false);
            status.setKEYEXIST(true);
            return status;
        }
    }

    public OperationStatus putNotOverWrite(byte[] key, byte[] value) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
        String valueBase64Encoded = Base64.getEncoder().encodeToString(value);

        String countQuery = LogUtils.stringFormatter("select count(*) from {} where KEY = to_binary('{}', " +
            "'BASE64')", tableName, keyBase64Encoded);

        OperationStatus res = connection.executeUpdate(countQuery);
        int rows = (int) res.getResult();

        if (rows == 0) {
            String insertStatement = "INSERT INTO " + tableName
                + " (KEY, VALUE) VALUES (to_binary('" + keyBase64Encoded + "', 'BASE64'), " +
                "to_binary('" + valueBase64Encoded + "', 'BASE64'))";

            log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
                "put(encoded)", tableName, keyBase64Encoded, valueBase64Encoded, insertStatement));

            OperationStatus result = connection.executeUpdate(insertStatement);

            // Now that INSERT operation is done, we need to order the data
            if (tableOrdered)
                orderTable();

            return result;
        }
        else {
            OperationStatus status = new OperationStatus(false);
            status.setKEYEXIST(true);
            log.severe(LogUtils.stringFormatter("Unable to put key={} & value={} and key={}" +
                " exists", keyBase64Encoded, valueBase64Encoded, keyBase64Encoded));

            return status;
        }
    }

    public OperationStatus delete(StaticBuffer key) {

        String statement = "TRUNCATE * FROM " + tableName + " WHERE KEY = " + key;

        return connection.executeUpdate(statement);
    }

    public OperationStatus delete(byte[] key) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);

        String statement = "DELETE FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
                "delete(encoded)", tableName, keyBase64Encoded, statement));

        OperationStatus result = connection.executeUpdate(statement);

        // Now that DELETE operation is done, we need to re-order the data
        if (tableOrdered)
            orderTable();

        return result;
    }

    public OperationStatus get(String key) throws SQLException
    {
        String qry = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64') limit 1";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
                "get(encoded)", tableName, key, qry));

        return connection.executeQuery(qry);
    }

    public KeyValueEntry get(String key, String value) throws SQLException
    {
        String qry = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64') AND VALUE = to_binary('" + value + "', 'BASE64') limit 1";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
            "get(encoded)", tableName, key, qry));

        OperationStatus res = connection.executeQuery(qry);
        ResultSet resultSet = (ResultSet) res.getResult();
//        System.out.println("Query is : " + qry);
        KeyValueEntry entry = null;

        while (resultSet.next()) {
            String keyEncoded = resultSet.getString("KEY");
            String valueEncoded = resultSet.getString("VALUE");

            byte[] byteKey = getByteArray(keyEncoded);
            byte[] byteValue = getByteArray(valueEncoded);

            StaticBuffer keySB = getBuffer(byteKey);
            StaticBuffer valueSB = getBuffer(byteValue);

            entry = new KeyValueEntry(keySB, valueSB);
        }

        return entry;
    }

    public OperationStatus get(byte[] key) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);

        String statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
                "get(encoded)", tableName, keyBase64Encoded, statement));
//        System.out.println("Query is: " + statement);
        return connection.executeQuery(statement);
    }

    public OperationStatus getAll() {
        String statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName;
        return connection.executeQuery(statement);
    }

    public OperationStatus getAll(Integer limit) {
        String statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName + " LIMIT " + limit;
        return connection.executeQuery(statement);
    }

    public void close() throws PermanentBackendException {

//        System.out.println("Closing DB connection for table " + tableName);
        OperationStatus status = connection.close();
        log.info("Successfully closed connection from SnowFlake DB class");
//        System.out.println("Successfully closed connection to DB DAO " + tableName);
        if (!status.getStatus()) {
            throw new PermanentBackendException("Could not close connection to Graph: " + status);
        }
    }

    public SnowFlakeCursor openCursor(){ //} throws IOException {
        SnowFlakeCursor cursor = new SnowFlakeCursor(getConnection(), tableOrdered);
        cursor.setTable(tableName);
//        try {
//            initializeDatabase();
//        } catch (Exception e) {
//            throw new IOException(e);
//        }

        log.info("Successfully created cursor connection to SnowFlake using custom SnowFlakeCursor class");
        return cursor;
    }

    private void initializeDatabase() throws Exception {

        SnowFlakeDatabase sdb = new SnowFlakeDatabase(tableName, transaction);
        sdb.close();
    }

    public OperationStatus getTables(String dbName, String schema)
    {
        //String tableName = "GRAPHDBTEST1.demo";
        try{
            String listOfTableQry = "show tables in "+dbName +"."+schema;

            OperationStatus result = connection.executeQuery(listOfTableQry);
//            System.out.println(listOfTableQry);
            ResultSet resultSet = ((ResultSet) result.getResult());

            List<String> tableList = new ArrayList<String>();

            while(resultSet.next())
            {
                tableList.add(resultSet.getString("name"));
//                System.out.println(resultSet.getString("name"));
                // System.out.println("row " + rowIdx + ", column 0: " +
                //                 resultSet.getString("name"));
            }
            // statement.executeUpdate(listOfTableQry);
//            System.out.println(tableList.size());

            OperationStatus status = new OperationStatus(true);
            status.setResult(tableList);
            log.info("Successfully fetched list of tables as " + tableList.toString());

            return status;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    //remove tables
    public OperationStatus removeTable(String schema, String tableName)
    {
        try{
            String dropTable = "drop table if exists "+schema +"."+tableName;
//            ResultSet resultSet = connection.executeQuery(dropTable);
            OperationStatus result = connection.executeQuery(dropTable);
            ResultSet resultSet = (ResultSet) result.getResult();

            //System.out.println(" why "+result);
            while(resultSet.next())
            {
//                System.out.println(resultSet.getString(1));
                return new OperationStatus(true);
            }
//            statement.close();
//            connection.close();
            // return result;

            log.info("Successfully removed table " + tableName);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            log.severe(LogUtils.logTraceback("Unable to remove table " + tableName, e));
        }
        return new OperationStatus(true);
    }

    public String getTableName() {
        return tableName;
    }
}
