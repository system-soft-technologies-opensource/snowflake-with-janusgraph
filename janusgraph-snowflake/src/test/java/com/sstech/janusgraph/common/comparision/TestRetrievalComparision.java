package com.sstech.janusgraph.common.comparision;

import com.google.common.collect.Lists;
import com.sleepycat.je.*;
import com.sstech.janusgraph.common.comparision.datafetcher.BerkleyJEDataFetcher;
import com.sstech.janusgraph.common.storage.BerkeleyDBStorageSetup;
import com.sstech.janusgraph.common.storage.BerkleyDBStorageClassSetup;
import com.sstech.janusgraph.common.storage.SnowFlakeDBStorageClassSetup;
import com.sstech.janusgraph.common.utils.DataLoader;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeKeyValueStore;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.berkeleyje.BerkeleyJEKeyValueStore;
import org.janusgraph.diskstorage.berkeleyje.BerkeleyJETx;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KVQuery;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.util.RecordIterator;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestRetrievalComparision {
    /*
    This Class is for doing comparative assertions. That is, it does the same operation on both SnowFlakeDB and
    BerkleyDB JE, and does assertions against the returned values of each operation.

    Pre Execution:
    Setup StorageManager, KeyValueStore, Transaction class for both SnowFlakeDB and BerkleyDB.
    Fetch sample 20 records from BerkleyDB.

    1: Assert the result of get operation on both sources for each KEY in fetched sample records
    2: Assert the result of getSlice operation on both sources for custom KVQuery (Custom start/end in list of sample records)
        in fetched sample records
    3: TODO: Assert the result of delete operation. Once delete operation is done, do get OP and assert its response.
    4: TODO: Assert the result of insert operation. Once insert operation is done, do get OP and assert the response.
    5: TODO: Assert the result of containsKey operation.
    6: TODO: Assert the result of getSlices operation.
    7: TODO: Assert the result of mutateMany operation.
    */

    // Generic
    String graphPath = BerkeleyDBStorageSetup.getDataPath();
    private String tableName = "edgestore";
    Integer recordLimit = -1;

    private SnowFlakeKeyValueStore sfStore;
    private SnowFlakeTransaction sfTx;
    private BerkeleyJEKeyValueStore bdbStore;
    private BerkeleyJETx bdbTxh;
    private Cursor bdbCur;

    private BerkleyJEDataFetcher dataFetcher;
    private DataLoader loader;

    private SnowFlakeDBStorageClassSetup sfDBStorageSetup;
    private BerkleyDBStorageClassSetup bdbStorageSetup;

    private List<KeyValueEntry> sampleRecords;

    public TestRetrievalComparision() {

    }

    @Before
    public void setUp() throws Exception {
        sfDBStorageSetup = new SnowFlakeDBStorageClassSetup(tableName);
        sfDBStorageSetup.initializeSFEngine();

        sfStore = sfDBStorageSetup.getStore();
        sfTx = sfDBStorageSetup.getTransaction();
        System.out.println("Im setting up initial resources");
        bdbStorageSetup = new BerkleyDBStorageClassSetup(tableName);
        bdbStorageSetup.initializeBDBEngine();

        dataFetcher = new BerkleyJEDataFetcher(graphPath);
        dataFetcher.initializeDataFetchEngine(tableName);

        bdbStore = bdbStorageSetup.getStore();
        bdbTxh = bdbStorageSetup.getTransaction();
        bdbCur = bdbStorageSetup.getCursor();

        sampleRecords = dataFetcher.getTopKeyValueEntries(recordLimit);

        dataLoaderForSnowFlake();
    }

    private void dataLoaderForSnowFlake() throws Exception {
        loader = new DataLoader(tableName);
        loader.load(sampleRecords);
    }

    private KVQuery generateKVQuery(Integer start, Integer end) {
        KeyValueEntry startEntry = sampleRecords.get(start);
        StaticBuffer startKey = startEntry.getKey();

        KeyValueEntry endEntry = sampleRecords.get(end);
        StaticBuffer endKey = endEntry.getKey();

        return new KVQuery(startKey, endKey);
    }

    private KVQuery generateKVQuery(Integer start, Integer end, Integer limit) {
        KeyValueEntry startEntry = sampleRecords.get(start);
        StaticBuffer startKey = startEntry.getKey();

        KeyValueEntry endEntry = sampleRecords.get(end);
        StaticBuffer endKey = endEntry.getKey();

        return new KVQuery(startKey, endKey, limit);
    }

    private static StaticBuffer getBuffer(DatabaseEntry entry) {
        return new StaticArrayBuffer(entry.getData(),entry.getOffset(),entry.getOffset()+entry.getSize());
    }

    // This method compares the retrievals methods of multiple Stores currently BerkleyJE & SnowFlake
    @Test
    public void testGetOperationAcrossSources() {
        sampleRecords.forEach( record -> {
            StaticBuffer key = record.getKey();
            StaticBuffer sampleValue = record.getValue();
            StaticBuffer bdbValue = null;
            StaticBuffer sfValue = null;

            // Fetch the Value against given 'key' from BerkleyDB
            try {
                bdbValue = bdbStore.get(key, bdbTxh);
            } catch (BackendException e) {
                e.printStackTrace();
            }

            // Fetch the Value against given 'key' from SnowFlakeDB
            try {
                sfValue = sfStore.get(key, sfTx);
            } catch (BackendException e) {
                e.printStackTrace();
            }

            Assert.assertEquals(sampleValue, bdbValue);
            Assert.assertEquals(sampleValue, sfValue);
            Assert.assertEquals(sfValue, bdbValue);

        });
    }

    @Test
    public void testGetSliceOperationAcrossSources() throws Exception {
        KVQuery query = generateKVQuery(2, 8);

        RecordIterator<KeyValueEntry> bdbRecords = bdbStore.getSlice(query, bdbTxh);
        RecordIterator<KeyValueEntry> sfRecords = sfStore.getSlice(query, sfTx);

        ArrayList<KeyValueEntry> bdbRecordList = Lists.newArrayList(bdbRecords);
        ArrayList<KeyValueEntry> sfRecordList = Lists.newArrayList(sfRecords);

        Assert.assertEquals(bdbRecordList.size(), sfRecordList.size());

        for (int i = 0; i < bdbRecordList.size(); i++) {
            KeyValueEntry bdbRecord = bdbRecordList.get(i);
            KeyValueEntry sfRecord = sfRecordList.get(i);

            Assert.assertEquals(bdbRecord.getKey(), sfRecord.getKey());
            Assert.assertEquals(bdbRecord.getValue(), sfRecord.getValue());
        }

        query = generateKVQuery(2, 16, 5);

        bdbRecords = bdbStore.getSlice(query, bdbTxh);
        sfRecords = sfStore.getSlice(query, sfTx);

        bdbRecordList = Lists.newArrayList(bdbRecords);
        sfRecordList = Lists.newArrayList(sfRecords);

        Assert.assertEquals(bdbRecordList.size(), sfRecordList.size());

        for (int i = 0; i < bdbRecordList.size(); i++) {
            KeyValueEntry bdbRecord = bdbRecordList.get(i);
            KeyValueEntry sfRecord = sfRecordList.get(i);

            Assert.assertEquals(bdbRecord.getKey(), sfRecord.getKey());
            Assert.assertEquals(bdbRecord.getValue(), sfRecord.getValue());
        }
    }

    @Test
    public void testDeleteOperationAcrossSources() {    }

    @Test
    public void testInsertOperationAcrossSources() {    }

    @Test
    public void testContainsKeyOperationAcrossSources() {    }

    @Test
    public void testGetSlicesOperationAcrossSources() {     }

    @Test
    public void testMutateManyOperationAcrossSources() {    }


    @After
    public void tearDown() throws Exception {
        System.out.println("Closing out all opened resources");
        sfDBStorageSetup.closeSFResources();
        bdbStorageSetup.closeBDBResources();
        loader.close();
    }
}
