package com.sstech.janusgraph.common.utils;

import com.sstech.janusgraph.common.comparision.datafetcher.BerkleyJEDataFetcher;
import com.sstech.janusgraph.common.storage.BerkeleyDBStorageSetup;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;

import java.util.Arrays;
import java.util.List;

public class DataLoader {

    private String graphPath;
    private Integer recordLimit = -1;

    private String tableName;

    private BerkleyJEDataFetcher dataFetcher;
    private SnowFlakeDataLoader dataLoader;

    public DataLoader(String table) throws Exception {
        tableName = table;
        graphPath = BerkeleyDBStorageSetup.getDataPath();

        initializeBDBDataReader();
        initializeSnowFlakeDataLoader();
    }

    public void setGraphPath(String path) {
        graphPath = path;
    }

    public void setLimit(Integer limit) {
        recordLimit = limit;
    }

    private void initializeSnowFlakeDataLoader() throws Exception {
        dataLoader = new SnowFlakeDataLoader();
        dataLoader.initializeDataLoadEngine(tableName);
    }

    public void load(List<KeyValueEntry> values) {
        System.out.println("Im loading data into " + tableName);
        OperationStatus res = dataLoader.insertArray(values);
    }

    public void close() throws Exception {
        dataFetcher.closeResources();
        dataLoader.closeResources();
    }

    private void initializeBDBDataReader() throws PermanentBackendException {
        dataFetcher = new BerkleyJEDataFetcher(graphPath);
        dataFetcher.initializeDataFetchEngine(tableName);
    }

    public List<KeyValueEntry> getBerkleyDBRecords() {
        List<KeyValueEntry> records = dataFetcher.getTopKeyValueEntries(recordLimit);
        System.out.println("Total records fetched from BerkleyDB for table " + tableName + " is " + records.size());
        return records;
    }

    public static void main(String[] args) throws Exception {
        List<String> tables = Arrays.asList("edgestore_lock_", "txlog", "edgestore", "graphindex_lock_",
            "systemlog", "system_properties_lock_", "janusgraph_ids", "graphindex", "system_properties");

        for (String table : tables) {
            DataLoader loader = new DataLoader(table);
            loader.setGraphPath(BerkeleyDBStorageSetup.getDataPath());

            List<KeyValueEntry> entries = loader.getBerkleyDBRecords();

            loader.load(entries);

            System.out.println("Loaded " + table);
        }
    }
}
