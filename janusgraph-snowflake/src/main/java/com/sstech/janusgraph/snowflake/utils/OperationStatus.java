package com.sstech.janusgraph.snowflake.utils;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OperationStatus implements Serializable {
    public boolean SUCCESS = true;
    public boolean FAILED = false;

    public boolean KEYEXIST = true;

    private boolean operationStatus;
    private Object resultSet;

    public OperationStatus(boolean status) {
        operationStatus = status;
    }

    public void setKEYEXIST(boolean KEYEXIST) {
        this.KEYEXIST = KEYEXIST;
    }

    public void setResult(Object result) {
        resultSet = result;
    }

    public Object getResult() {
        return resultSet;
    }

    public int getResultSize() {
        if (resultSet instanceof ResultSet) {
            ResultSet res = (ResultSet) resultSet;
            int size = 0;
            try {
                while (res.next()) {
//                    System.out.println(size);
                    size += 1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
//                System.out.println("exit??");
                System.exit(-1);
            }
            return size;
        } else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public int getResultSize(Integer colNum) throws SQLException {
        if (resultSet instanceof ResultSet) {
            ResultSet res = (ResultSet) resultSet;
            int size;
            res.next();
//            System.out.println("Next done");
            size = res.getInt(colNum);
//            System.out.println("Got Count Int as " + size);
            return size;

//            try {
//                while (res.next()) {
//                    System.out.println(size);
//                    size += 1;
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//                System.out.println("exit??");
//                System.exit(-1);
//            }
//            return size;
        } else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public int getResultSize(String col) {
        List<Long> result = new ArrayList<>();

        if (resultSet instanceof ResultSet) {
            ResultSet res = (ResultSet) resultSet;
//            try {
//                System.out.println(res.getMetaData().getColumnName(0));
//                System.out.println(res.getMetaData().getColumnName(1));
//            } catch (SQLException e) {
//                e.printStackTrace();
//                System.out.println("Couldn't get colname");
//            }
            int size = 0;
            try {
                while (res.next()) {
                    result.add(res.getLong(col));
                }
            } catch (SQLException e) {
                e.printStackTrace();
//                System.exit(-1);
            }
            return result.get(0).intValue();
        } else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public ArrayList<String> getResultAsList(String columnName) {
        if (resultSet instanceof ResultSet) {
            ArrayList data = new ArrayList();
            try {
                while (((ResultSet) resultSet).next()) {
                    try {
                        data.add(((ResultSet) resultSet).getString(columnName));
                    } catch (SQLException e) {
                        e.printStackTrace();
                        System.exit(-1);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                System.exit(-2);
            }
            return data;
        }
        else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                    "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public ArrayList<ArrayList> getResultAsList(String ... columns) throws SQLException, UnsupportedOperationException {
        if (resultSet instanceof ResultSet) {
            ArrayList data = new ArrayList();
            while (((ResultSet) resultSet).next()) {
                ArrayList rowData = new ArrayList();

                for (String column : columns) {
                    rowData.add(((ResultSet) resultSet).getString(column));
                }
                data.add(rowData);
            }
            return data;
        }
        else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                    "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public ArrayList<String> getResultAsList(Integer columnIndex) throws SQLException, UnsupportedOperationException {
        if (resultSet instanceof ResultSet) {
            ArrayList data = new ArrayList();
            while (((ResultSet) resultSet).next()) {
                data.add(((ResultSet) resultSet).getString(columnIndex));
            }
            return data;
        }
        else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                    "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public ArrayList<ArrayList> getResultAsList(Integer ... columns) throws SQLException, UnsupportedOperationException {
        if (resultSet instanceof ResultSet) {
            ArrayList data = new ArrayList();
            while (((ResultSet) resultSet).next()) {
                ArrayList rowData = new ArrayList();

                for (Integer columnIdx : columns) {
                    rowData.add(((ResultSet) resultSet).getString(columnIdx));
                }
                data.add(rowData);
            }
            return data;
        }
        else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                    "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public void setOperationStatus(boolean status) {
        operationStatus = status;
    }

    public boolean getStatus() {
        return operationStatus;
    }

}
