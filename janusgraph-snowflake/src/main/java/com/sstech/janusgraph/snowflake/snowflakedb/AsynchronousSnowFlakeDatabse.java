package com.sstech.janusgraph.snowflake.snowflakedb;

import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.GraphUtils;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.common.utils.SingleFileLogger;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.PermanentBackendException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Logger;

public class AsynchronousSnowFlakeDatabse {
    private ExecutorService executorService;
    private String tableName;
//    private SnowFlakeConnection connection;

    private static SingleFileLogger logger = SingleFileLogger.getInstance("/var/log/janusgraph-snowflake/custom.log");
    private Logger log;

    private BaseTransactionConfig cfg = GraphUtils.getEmptyTxConfig();

    public AsynchronousSnowFlakeDatabse(ExecutorService executor, String tableName) throws SnowFlakeConnectionException {
        executorService = executor;
        this.tableName = tableName;
//        connection = tx.getConnection();
        log = logger.getLogger();

    }

    private SnowFlakeTransaction beginTransaction() throws BackendException {
        return new SnowFlakeTransaction(cfg, null);
    }

    private void closeTransaction(SnowFlakeTransaction tx) {
        tx.close();
    }

    public Future<OperationStatus> putAsynchronous(byte[] key, byte[] value) {
        Future<OperationStatus> futureResult = executorService.submit(new Callable<OperationStatus>() {
            public OperationStatus call() {
                // Default Status
                OperationStatus status=new OperationStatus(false);
                status.setResult(null);

                try {
                    SnowFlakeTransaction tx = beginTransaction();
                    status = put(key, value, tx);
                    closeTransaction(tx);
                } catch (BackendException e) {
                    e.printStackTrace();
                }
                return status;
            }
        });

        return futureResult;
    }

    public Future<OperationStatus> putNoOverwriteAsynchronous(byte[] key, byte[] value) {
        Future<OperationStatus> futureResult = executorService.submit(new Callable<OperationStatus>() {
            public OperationStatus call() {
                // Default Status
                OperationStatus status=new OperationStatus(false);
                status.setResult(null);

                try {
                    SnowFlakeTransaction tx = beginTransaction();
                    status = putNotOverWrite(key, value, tx);
                    closeTransaction(tx);
                } catch (BackendException e) {
                    e.printStackTrace();
                }
                return status;
            }
        });

        return futureResult;
    }

    public Future<OperationStatus> deleteAsynchronous(byte[] key) {
        Future<OperationStatus> futureResult = executorService.submit(new Callable<OperationStatus>() {
            public OperationStatus call() {
                // Default Status
                OperationStatus status=new OperationStatus(false);
                status.setResult(null);

                try {
                    SnowFlakeTransaction tx = beginTransaction();
                    status = delete(key, tx);
                    closeTransaction(tx);
                } catch (BackendException e) {
                    e.printStackTrace();
                }
                return status;
            }
        });

        return futureResult;
    }

    public Future<OperationStatus> getAsynchronous(byte[] key) {
        Future<OperationStatus> futureResult = executorService.submit(new Callable<OperationStatus>() {
            public OperationStatus call() {
                // Default Status
                OperationStatus status=new OperationStatus(false);
                status.setResult(null);

                try {
                    SnowFlakeTransaction tx = beginTransaction();
                    status = get(key, tx);
                    closeTransaction(tx);
                } catch (BackendException e) {
                    e.printStackTrace();
                }
                return status;
            }
        });

        return futureResult;
    }

    private Future<ResultSet> getCursorSearchKeyAsynchronous(byte[] key) {
        Future<ResultSet> futureResult = executorService.submit(new Callable<ResultSet>() {
            public ResultSet call() {
                ResultSet result=null;
                try {
                    SnowFlakeTransaction tx = beginTransaction();
                    result = getSearchKey(key, tx);
                    closeTransaction(tx);
                } catch (BackendException e) {
                    e.printStackTrace();
                }
                return result;
            }
        });
        return futureResult;
    }

    private ResultSet getSearchKey(byte[] key, SnowFlakeTransaction tx) throws BackendException {
        try {
            SnowFlakeConnection conn = tx.getConnection();

            log.info(LogUtils.stringFormatter("op=%s(async), tbl=%s, key=%s", "getSearchKey", tableName, key.toString()));

            String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
            log.info(LogUtils.stringFormatter("op=%s(async), tbl=%s, encodedKey=%s", "getSearchKey", tableName, keyBase64Encoded));

            String statement = "SELECT * FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";
            log.info(LogUtils.stringFormatter("op=%s(async), tbl=%s, sql=%s", "getSearchKey", tableName, statement));

            ResultSet resultSet = conn.executeCursorQuery(statement);

            try {
                log.info(LogUtils.stringFormatter("op=%s(async), tbl=%s, sql=%s, columnCount=%s", "getSearchKey", tableName, statement, String.valueOf(resultSet.getMetaData().getColumnCount())));
            } catch (SQLException e) {
                log.severe("Unable to run query " + statement);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            return resultSet;
        } catch (SnowFlakeConnectionException e) {
            throw new PermanentBackendException(LogUtils.logTraceback(LogUtils.stringFormatter("op=%s(async), db=%s, " +
                    "status=Conn Failed", "getSearchKey", tableName), e));
        }
    }

    private OperationStatus put(byte[] key, byte[] value, SnowFlakeTransaction tx) throws BackendException {
        try {
            SnowFlakeConnection conn = tx.getConnection();

            String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
            String valueBase64Encoded = Base64.getEncoder().encodeToString(value);

            // This currently is generic put, without checking if entry exists in DB. Need to implement
            // putNoOverWrite which shall insert only when data is absent else throw error
            String insertStatement = "INSERT INTO " + tableName
                    + " (KEY, VALUE) VALUES (to_binary('" + keyBase64Encoded + "', 'BASE64'), " +
                    "to_binary('" + valueBase64Encoded + "', 'BASE64'))";

            log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s(async), tbl=%s, key=%s, value=%s, sql=%s",
                    "put(encoded)", tableName, keyBase64Encoded, valueBase64Encoded, insertStatement));

            return conn.executeUpdate(insertStatement);
        } catch (SnowFlakeConnectionException e) {
            throw new PermanentBackendException(LogUtils.logTraceback(LogUtils.stringFormatter("op=%s(async), db=%s, " +
                    "status=Conn Failed", "put", tableName), e));
        }
    }

    private OperationStatus putNotOverWrite(byte[] key, byte[] value, SnowFlakeTransaction tx) throws BackendException {
        try {
            SnowFlakeConnection conn = tx.getConnection();

            String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
            String valueBase64Encoded = Base64.getEncoder().encodeToString(value);

            String statement = "SELECT COUNT(*) FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

            if ((Integer) conn.executeQuery(statement).getResult() == 0) {
                String insertStatement = "INSERT INTO " + tableName
                        + " (KEY, VALUE) VALUES (to_binary('" + keyBase64Encoded + "', 'BASE64'), " +
                        "to_binary('" + valueBase64Encoded + "', 'BASE64'))";

                log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s(async), tbl=%s, key=%s, value=%s, sql=%s",
                        "putNotOverWrite(encoded)", tableName, keyBase64Encoded, valueBase64Encoded, insertStatement));

                return conn.executeUpdate(insertStatement);
            } else {
                OperationStatus status = new OperationStatus(false);
                status.setKEYEXIST(true);
                log.severe(LogUtils.stringFormatter("(async): Unable to put key=%s & value=%s and key=%s exists",
                        keyBase64Encoded, valueBase64Encoded, keyBase64Encoded));
                return status;
            }
        } catch (SnowFlakeConnectionException e) {
            throw new PermanentBackendException(LogUtils.logTraceback(LogUtils.stringFormatter("op=%s(async), db=%s, " +
                    "status=Conn Failed", "putNotOverWrite", tableName), e));
        }
    }

    private OperationStatus delete(byte[] key, SnowFlakeTransaction tx) throws BackendException {
        try {
            SnowFlakeConnection conn = tx.getConnection();

            String keyBase64Encoded = Base64.getEncoder().encodeToString(key);

            String statement = "DELETE FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

            log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s(async), tbl=%s, key=%s, sql=%s",
                    "delete(encoded)", tableName, keyBase64Encoded, statement));

            return conn.executeUpdate(statement);
        } catch (SnowFlakeConnectionException e) {
            throw new PermanentBackendException(LogUtils.logTraceback(LogUtils.stringFormatter("op=%s(async), db=%s, " +
                    "status=Conn Failed", "delete", tableName), e));
        }

    }

    private OperationStatus get(byte[] key, SnowFlakeTransaction tx) throws BackendException {
        try {
            SnowFlakeConnection conn = tx.getConnection();

            String keyBase64Encoded = Base64.getEncoder().encodeToString(key);

            String statement = "SELECT VALUE FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

            log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s(async), tbl=%s, key=%s, sql=%s",
                    "get(encoded)", tableName, keyBase64Encoded, statement));

            return conn.executeQuery(statement);

        } catch (SnowFlakeConnectionException e) {
            throw new PermanentBackendException(LogUtils.logTraceback(LogUtils.stringFormatter("op=%s(async), db=%s, " +
                    "status=Conn Failed", "delete", tableName), e));
        }
    }

}
