package com.sstech.janusgraph.snowflake.snowflakedb;

import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.common.utils.SingleFileLogger;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.StaticBuffer;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Logger;

//import com.sstech.janusgraph.common.utils.LogMe;

public class SnowFlakeCursor implements Serializable {
    private SnowFlakeConnection connection;
    private SnowFlakeTransaction txh;
    private String tableName;
    private boolean tableOrdered;

//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeCursor.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
    private static SingleFileLogger logger = SingleFileLogger.getInstance();
    private Logger log = logger.getLogger();

    public SnowFlakeCursor(SnowFlakeConnection snowFlakeConnection) {
//        log = logger.getLogger();
//        try {
//            log = logger.getLogger();
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new IOException(LogUtils.stringFormatter("Unable to create log file %s while executing", logger.getLogFileName()));
//        }

        connection = snowFlakeConnection;
    }

    public SnowFlakeCursor(SnowFlakeConnection snowFlakeConnection, boolean ordered) {
        tableOrdered = ordered;
        connection = snowFlakeConnection;
    }

    @Override
    public String toString() {
        return tableName;
    }

    public void setTable(String table) {
        tableName = table;
    }

    private SnowFlakeConnection getConnection() {
        return connection;
    }

    public ResultSet getSearchKey(StaticBuffer key) {
        String statement = "SELECT * FROM " + tableName + " WHERE KEY = " + key.toString();

        ResultSet resultSet = connection.executeCursorQuery(statement);
        return resultSet;
    }

    public ResultSet getSearchKey(byte[] key) throws BackendException  {
        log.info(LogUtils.stringFormatter("op=%s, tbl=%s, key=%s", "getSearchKey", tableName, key.toString()));

        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
        log.info(LogUtils.stringFormatter("op=%s, tbl=%s, encodedKey=%s", "getSearchKey", tableName, keyBase64Encoded));

        String statement = "SELECT * FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";
        log.info(LogUtils.stringFormatter("op=%s, tbl=%s, sql=%s", "getSearchKey", tableName, statement));
//        System.out.println("Now executing query " + statement);
        ResultSet resultSet = connection.executeCursorQuery(statement);
//
//        try {
//            log.info(LogUtils.stringFormatter("op=%s, tbl=%s, sql=%s, columnCount=%s", "getSearchKey", tableName, statement, String.valueOf(resultSet.getMetaData().getColumnCount())));
//        } catch (SQLException e) {
//            log.severe("Unable to run query " + statement);
//        } catch (NullPointerException e) {
//            throw new PermanentBackendException("Unable to run getSearchKey as log isn't present");
//        }

        return resultSet;
    }

    private DatabaseEntry getAsDatabaseEntry(String ... columns) {
        if (columns.length == 3) {
            String keyEncoded = columns[0];
            String valueEncoded = columns[1];
            String columnEncoded = columns[2];

            DatabaseEntry entry = new DatabaseEntry(toString(), getConnection(), keyEncoded, valueEncoded, columnEncoded);

            return entry;
        }
        else {
            String keyEncoded = columns[0];
            String valueEncoded = columns[1];

            DatabaseEntry entry = new DatabaseEntry(toString(), getConnection(), keyEncoded, valueEncoded);

            return entry;
        }

    }

    public List<DatabaseEntry> getKeyRange(byte[] startKey, byte[] endKey) throws SQLException {
        String startEncoded = Base64.getEncoder().encodeToString(startKey);
        String endEncoded = Base64.getEncoder().encodeToString(endKey);

        String statement;
        if (tableOrdered)
            statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE, " +
            "TO_VARCHAR(COLUMN1,'BASE64') as COL FROM " + tableName + " " +
            "WHERE KEY >= to_binary('" + startEncoded + "', 'BASE64') AND KEY <= to_binary('" + endEncoded + "', 'BASE64')";
        else
            statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE, " +
                "TO_VARCHAR(COLUMN1,'BASE64') as COL FROM " + tableName + " " +
                "WHERE KEY >= to_binary('" + startEncoded + "', 'BASE64') " +
                "AND KEY <= to_binary('" + endEncoded + "', 'BASE64') ORDER BY KEY ASC";

//        System.out.println("SQL is " + statement);
//        if (tableName.equals("edgestore") || tableName.equals("graphindex") || tableName.equals("janusgraph_ids"))
//            System.out.println("For the specified SliceQuery, SnowSQL query is " + statement);

        ResultSet result = connection.executeCursorQuery(statement);

        List<DatabaseEntry> entries = new ArrayList<>();

        while (result.next()) {
            String keyEncoded = result.getString("KEY");
            String valueEncoded = result.getString("VALUE");
            String columnEncoded = result.getString("COL");

            DatabaseEntry entry = getAsDatabaseEntry(keyEncoded, valueEncoded, columnEncoded);

            entries.add(entry);

        }

        if (result == null)
            System.out.println("Failed to execute For edgestore query " + statement + " number of rows " + entries.size());

        return entries;
    }

    public List<DatabaseEntry> getKey(byte[] key) throws SQLException {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);

        String statement;
        if (tableOrdered)
            statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE, " +
                "TO_VARCHAR(COLUMN1,'BASE64') as COL FROM " + tableName + " " +
                "WHERE KEY >= to_binary('" + keyBase64Encoded + "', 'BASE64')";
        else
            statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE, " +
                "TO_VARCHAR(COLUMN1,'BASE64') as COL FROM " + tableName + " " +
                "WHERE KEY >= to_binary('" + keyBase64Encoded + "', 'BASE64') ORDER BY KEY ASC";

//        System.out.println("SQL is " + statement);

        ResultSet result = connection.executeCursorQuery(statement);

        List<DatabaseEntry> entries = new ArrayList<>();

        while (result.next()) {
            String keyEncoded = result.getString("KEY");
            String valueEncoded = result.getString("VALUE");
            String columnEncoded = result.getString("COL");

            DatabaseEntry entry = getAsDatabaseEntry(keyEncoded, valueEncoded, columnEncoded);

            entries.add(entry);
        }

        return entries;
    }

    public void close() {
        connection.close();
    }
}
