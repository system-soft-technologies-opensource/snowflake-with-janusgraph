package com.sstech.janusgraph.diskstorage.snowflake;

//import com.sstech.janusgraph.common.KeyColumnValueStoreTest;
import com.sstech.janusgraph.common.storage.SnowFlakeDBStorageSetup;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.keycolumnvalue.KeyColumnValueStoreManager;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.OrderedKeyValueStoreManagerAdapter;
import org.junit.Test;

import org.janusgraph.diskstorage.KeyColumnValueStoreTest;

public class SnowFlakeVariableLengthKCVSTest extends KeyColumnValueStoreTest {

    public KeyColumnValueStoreManager openStorageManager() throws BackendException {
        SnowFlakeStoreManager sm = new SnowFlakeStoreManager(SnowFlakeDBStorageSetup.getSnowFlakeDBConfiguration());
        return new OrderedKeyValueStoreManagerAdapter(sm);
    }

    @Test
    @Override
    public void testConcurrentGetSlice() {

    }

    @Test @Override
    public void testConcurrentGetSliceAndMutate() {

    }
}
