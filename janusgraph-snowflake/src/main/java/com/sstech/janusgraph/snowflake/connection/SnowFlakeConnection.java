package com.sstech.janusgraph.snowflake.connection;

import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.common.utils.SingleFileLogger;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeCreateStatement;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;


public class SnowFlakeConnection implements Serializable {

    private static SnowFlakeConnection instance = null;

    private String URL = "";
    private Integer port = 0;
    private String warehouse = "";
    private String extra = "";
    private String userName = "";
    private String pwd = "";
    private String schema = "";
    private String db = "";
    private String role = "";

    private String connectionURL = "";
    private Connection snowflakeJdbcConnection;
    private SnowFlakeCreateStatement createStatement;

    private static SingleFileLogger logger = SingleFileLogger.getInstance();
    private Logger log = logger.getLogger();

    private boolean autoCommit = false;

    public SnowFlakeConnection(boolean auto_commit) throws IOException {

        autoCommit = auto_commit;
    }

    public static SnowFlakeConnection getInstance(boolean auto_commit) throws IOException {
        if (instance == null) {
            instance = new SnowFlakeConnection(auto_commit);
        }
        return instance;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String path) {
        URL = path;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setPort(Integer portNumber) {
        port = portNumber;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setWarehouse(String warehouse_name) {
        warehouse = warehouse_name;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setOthers(String others) {
        extra = others;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setUserName(String userName) {
        this.userName = userName;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setPassword(String pwd) {
        this.pwd = pwd;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setDB(String DB) {
        this.db = DB;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public String getDB() {
        return db;
    }
    public String getSchema() {
        return schema;
    }
    public void setSchema(String schema) {
        this.schema = schema;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setRole(String role) {
        this.role = role;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    private boolean areParametersSet() {
        return !URL.equals("") && port != 0 && !warehouse.equals("");
    }

    private void connect() throws SQLException {
        // build connection properties
        Properties properties = new Properties();

        properties.put("user", this.userName);        // replace "" with your user name
        properties.put("password", this.pwd);    // replace "" with your password
        properties.put("warehouse", this.warehouse);   // replace "" with target warehouse name
        properties.put("db", this.db);          // replace "" with target database name
        properties.put("schema", this.schema);      // replace "" with target schema name
        properties.put("role", this.role);
        String connectStr = this.URL;
        //properties.put("tracing", "on"); // optional tracing property

        if(connectStr == null)
        {
         connectStr = "jdbc:snowflake://ik55883.east-us-2.azure.snowflakecomputing.com"; // replace accountName with your account name
        }
        snowflakeJdbcConnection = DriverManager.getConnection(connectStr, properties);
    }

    private void buildConnectionURL() {
        connectionURL = URL;
    }

    private void buildConnection() throws SQLException, IOException {
        this.connect();
        createStatement = new SnowFlakeCreateStatement(snowflakeJdbcConnection);
        log.info("Successfully created 'createStatement' for JDBC connection to execute queries");
    }

    public SnowFlakeConnection getConnection() throws SQLException, IOException {
        this.buildConnection();
        return this;
    }

    public SnowFlakeCreateStatement getStatement() {
        return createStatement;
    }

    public OperationStatus executeUpdate(String sql) {
        log.info("Executing SQL update " + sql);
        try {
            OperationStatus status = new OperationStatus(true);
            status.setResult(createStatement.executeUpdate(sql));
            log.info(LogUtils.stringFormatter("Successfully executed update %s", sql));
            return status;
        }
        catch (SQLException e) {
            log.severe("Unable to run update " + sql);
            return  new OperationStatus(false);
        }
    }

    public OperationStatus executeQuery(String sql) {
        log.info("Executing SQL query " + sql);
        try {
            OperationStatus status = new OperationStatus(true);
            status.setResult(createStatement.executeQuery(sql));
            log.info(LogUtils.stringFormatter("Successfully executed query %s", sql));
            return status;
        }
        catch (SQLException e) {
            e.printStackTrace();
            System.exit(-10);
            log.severe("Unable to run query " + sql);
            return  new OperationStatus(false);
        }
    }

    public ResultSet executeCursorQuery(String sql) {
        log.info("Executing cursor query " + sql);
//        System.out.println("Executing query " + sql);
        try {
            ResultSet resultSet = createStatement.executeQuery(sql);
//            log.info(LogUtils.stringFormatter("Number of columns fetched executing %s SQL is %s", sql, String.valueOf(resultSet.getMetaData().getColumnCount())));
            return resultSet;
        }
        catch (SQLException e) {
//            System.out.println("Exception occurred in executing " + sql);
//            e.printStackTrace();
            return null;
        }
    }

    public OperationStatus close() {
        log.info("Closing connection to SnowFlake");
        try {
            createStatement.close();
//            System.out.println("Closed connection to create statement");
            log.info("Closed connection from createStatement");
            snowflakeJdbcConnection.close();
//            System.out.println("Closed jdbc connection ");
            log.info("Closed JDBC connection");
            return new OperationStatus(true);
        }
        catch (SQLException e) {
            log.severe("Couldn't close connection to SnowFlakeDB");
            return new OperationStatus(false);
        }
    }

    @Override
    public String toString() {
        return URL;
    }
}
