//package com.sstech.janusgraph.test;
//
//import static org.junit.Assert.*;
//
//import java.time.Instant;
//
//import org.janusgraph.diskstorage.BaseTransactionConfig;
//import org.janusgraph.diskstorage.configuration.ConfigOption;
//import org.janusgraph.diskstorage.configuration.Configuration;
//import org.janusgraph.diskstorage.util.time.TimestampProvider;
//import org.junit.Test;
//
//import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager;
//import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
//import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
//import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
//
//public class TestSnowFlakeDatabase {
//
//		BaseTransactionConfig cfg = new BaseTransactionConfig() {
//        @Override
//        public Instant getCommitTime() {
//            return null;
//        }
//
//        @Override
//        public void setCommitTime(Instant instant) {
//
//        }
//
//        @Override
//        public boolean hasCommitTime() {
//            return false;
//        }
//
//        @Override
//        public TimestampProvider getTimestampProvider() {
//            return null;
//        }
//
//        @Override
//        public String getGroupName() {
//            return null;
//        }
//
//        @Override
//        public boolean hasGroupName() {
//            return false;
//        }
//
//        @Override
//        public <V> V getCustomOption(ConfigOption<V> configOption) {
//            return null;
//        }
//
//        @Override
//        public Configuration getCustomOptions() {
//            return null;
//        }
//    };
//
//
//		SnowFlakeStoreManager manager = null;
//	    SnowFlakeTransaction tx = null;
//	    SnowFlakeDatabase sfd = null;
//
//	    public TestSnowFlakeDatabase () throws Exception
//		{
//	    	 manager = new SnowFlakeStoreManager(null);
//		     tx = new SnowFlakeTransaction(cfg, manager);
//		     sfd = new SnowFlakeDatabase("janusgraph_ids",tx);
//	   }
//
//	     @Test
//	     public void testgetConnection()
//	     {
//	    	 SnowFlakeConnection connection = sfd.getConnection();
//	    	// System.out.println(connection);
//	    	 assertNotNull(connection);
//	    	// assertNull(connection);
//
//	     }
//
//
//}
