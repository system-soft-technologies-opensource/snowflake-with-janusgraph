package com.sstech.janusgraph.snowflake.connection;

import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.common.utils.SingleFileLogger;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions;
import org.janusgraph.diskstorage.PermanentBackendException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Logger;

//import com.sstech.janusgraph.common.utils.LogMe;

public class SnowFlakeConnectionManager implements Serializable {

    private static SnowFlakeConnectionManager instance = null;

    private Integer commitTime;
    private Long startTime;
    private String URL = "";
    private Integer port = 0;
    private String userName = "";
    private String pwd = "";
    private String warehouse = "";
    private String DB = "";
    private String schema = "";
    private String role = "";
    private HashMap<String, String> extra = new HashMap<>();
    private SnowFlakeConnection connection;
    private boolean isConnected = false;

    private Boolean isLogged = true;

//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeConnectionManager.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
    private static SingleFileLogger logger = SingleFileLogger.getInstance();
    private Logger log = logger.getLogger();

    private static String dbKey = SnowFlakeDBConfigOptions.GRAPH.getName();
    private static String schemaKey = SnowFlakeDBConfigOptions.SCHEMA.getName();
    private static String URLKey = "URL";
    private static String roleKey = "role";
    private static String userKey = "userName";
    private static String pwdKey = "password";
    private static String wareHouseKey = "warehouse";


    public SnowFlakeConnectionManager (Integer commit_time) throws PermanentBackendException {
        commitTime = commit_time;
        startTime = System.currentTimeMillis();
    }

    public SnowFlakeConnectionManager (String connectionFilePath) throws PermanentBackendException {
        try {
            InputStream inputHandler = new FileInputStream(connectionFilePath);
            Properties credentialsProperties = new Properties();

            credentialsProperties.load(inputHandler);

            loadConnectionProperties(credentialsProperties);

        } catch (IOException e) {
            e.printStackTrace();
            throw new PermanentBackendException(LogUtils.stringFormatter("Unable to locate Cluster Connection File with SnowFlake credentials (%s)", connectionFilePath));
        }

        startTime = System.currentTimeMillis();

    }

    public SnowFlakeConnectionManager (Properties connectionProperties) throws PermanentBackendException {
        loadConnectionProperties(connectionProperties);
        startTime = System.currentTimeMillis();
    }

    private void loadConnectionProperties(Properties connectionProperties) {
        setURL(connectionProperties.getProperty(URLKey));
        setUserName(connectionProperties.getProperty(userKey));
        setPassword(connectionProperties.getProperty(pwdKey));
        setWarehouse(connectionProperties.getProperty(wareHouseKey));
        setDB(connectionProperties.getProperty(dbKey));
        setSchema(connectionProperties.getProperty(schemaKey));
        setRole(connectionProperties.getProperty(roleKey));

    }

    public static SnowFlakeConnectionManager getInstance(Integer commit_time) throws PermanentBackendException {
        if (instance == null) {
            instance = new SnowFlakeConnectionManager(commit_time);
        }
        return instance;
    }

    public static SnowFlakeConnectionManager getInstance(String clusterFilePath) throws PermanentBackendException {
        if (instance == null) {
            instance = new SnowFlakeConnectionManager(clusterFilePath);
        }
        return instance;
    }

    public static SnowFlakeConnectionManager getInstance(Properties connectionProperties) throws PermanentBackendException {
        if (instance == null) {
            instance = new SnowFlakeConnectionManager(connectionProperties);
        }
        return instance;
    }

    protected void autoTimer() {

    }

    public void setURL(String path) {
        URL = path;
        if (isLogged)
            log.info("Set JDBC URL = " + URL);
    }
    public void setPort(Integer portNumber) {
        port = portNumber;
        if (isLogged)
            log.info("Set Port number = " + port);
    }
    public void setWarehouse(String warehouse_name) {
        warehouse = warehouse_name;
        if (isLogged)
            log.info("Set warehouse as " + warehouse);
    }
    public void setUserName(String username) {
        this.userName = username;
        if (isLogged)
            log.info("Set userName = " + username);
    }
    public void setDB(String DB) {
        this.DB = DB;
        if (isLogged)
            log.info("Set DB = " + DB);
    }
    public void setSchema(String schema) {
        this.schema = schema;
        if (isLogged)
            log.info("Set Schema = " + schema);
    }
    public void setRole(String role) {
        this.role = role;
        if (isLogged)
            log.info("Set access role = " + role);
    }
    public void setPassword(String pwd) {
        this.pwd = pwd;
        if (isLogged)
            log.info("Set password as = " + pwd);
    }
    public void setOthers(String propName, String propVal) {
        extra.putIfAbsent(propName, propVal);

//        if (areParametersSet()) {
//            connect();
//        }
    }

    public SnowFlakeConnection getConnection() throws SnowFlakeConnectionException {
        if (isConnected) {
            log.info("Connected to SnowFlake successfully");
            return connection;
        }
        else {
            throw new SnowFlakeConnectionException("All the required connection parameters " +
                    "(URL, port, warehouse) are not set before fetching connection");
        }
    }

    private boolean areParametersSet() {
        if (URL.equals("") && warehouse.equals("") && userName.equals("") && pwd.equals("") && schema.equals("")) {
            return false;
        }
        else {
            return true;
        }
    }

    public void connect()  throws SnowFlakeConnectionException {
        try {
//            SnowFlakeConnection snowFlakeConnection = SnowFlakeConnection.getInstance(false);
            SnowFlakeConnection snowFlakeConnection = new SnowFlakeConnection(false);

            snowFlakeConnection.setURL(URL);
            snowFlakeConnection.setPort(port);
            snowFlakeConnection.setWarehouse(warehouse);
            snowFlakeConnection.setUserName(userName);
            snowFlakeConnection.setPassword(pwd);
            snowFlakeConnection.setDB(DB);
            snowFlakeConnection.setRole(role);
            snowFlakeConnection.setSchema(schema);

            isConnected = true;

//        snowFlakeConnection.setOthers(extra);
            connection = snowFlakeConnection.getConnection();
        }
        catch (SQLException e) {
            e.printStackTrace();
            throw new SnowFlakeConnectionException("Couldn't connect to SnowFlake DB with properties URL: " + URL +
                    " user: " + userName + " password: " + pwd + " warehouse: " + warehouse + " role: " + role +
                    " DB: " + DB + " schema: " + schema);
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new SnowFlakeConnectionException("Unable to create log file at " + logger.getLogFileName() + " aborting connection to SnowFlake");
        }
    }

    public void close() {
//        System.out.println("Closing snowflake connection manager");
        connection.close();
    }
}
