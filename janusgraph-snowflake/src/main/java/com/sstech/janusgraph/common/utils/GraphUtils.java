package com.sstech.janusgraph.common.utils;

import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.util.StandardBaseTransactionConfig;
import org.janusgraph.diskstorage.util.time.TimestampProviders;

import java.util.Arrays;
import java.util.List;

public final class GraphUtils {

    public static String BDB_GRAPH_PATH = GraphUtils.generateBDBGraphPath();
    public static String SF_CREDENTIALS_FILE = GraphUtils.generateSnowFlakeClusterFilePath();

    public static List<String> TABLES_TO_OPERATE_ON = Arrays.asList("edgestore", "graphindex", "janusgraph_ids", "systemlog");
    public static List<String> TABLES_TO_SKIP = Arrays.asList("systemlog", "txlog");

    static private String generateBDBGraphPath() {
        if (OSUtils.isWindows()) {
            return "D:\\Projects\\Projects\\Freelancing\\Elysium Analytics\\snowflake\\BerkleyGraphs\\graph1";
        }
        else if (OSUtils.isUnix()) {
            return "/root/data/graph";
        }
        else
            return "/root/data/graph";
    }

    static private String generateSnowFlakeClusterFilePath() {
        if (OSUtils.isWindows()) {
//            return "D:\\Projects\\Projects\\Freelancing\\Elysium Analytics\\snowflake\\janus-snowflake\\janusgraph-snowflake\\src\\main\\resources\\credentials\\sf-cluster-debasish.properties";
            return "D:\\Projects\\Projects\\Freelancing\\Elysium Analytics\\snowflake\\sf-cluster-debasish.properties";
        }
        else if (OSUtils.isUnix()) {
            return "/root/debasish/credentials/sf-cluster-debasish.properties";
        }
        else
            return "sf-cluster-debasish.properties";
    }

    public static BaseTransactionConfig getEmptyTxConfig() {
        return StandardBaseTransactionConfig.of(TimestampProviders.MICRO);
    }

}
