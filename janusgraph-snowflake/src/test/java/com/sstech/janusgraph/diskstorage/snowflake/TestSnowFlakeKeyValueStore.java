package com.sstech.janusgraph.diskstorage.snowflake;

import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.storage.SnowFlakeDBStorageSetup;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

//public class TestSnowFlakeKeyValueStore extends KeyValueStoreTest {
public class TestSnowFlakeKeyValueStore {

	private String tableName = "system_properties";
	private SnowFlakeStoreManager manager;
	private SnowFlakeKeyValueStore keyValueStore;
	private SnowFlakeTransaction txh;

	public TestSnowFlakeKeyValueStore() throws Exception
	{
	     manager = openStorageManager();
	     txh = manager.getTransaction();
		 SnowFlakeDatabase db = openDatabase();
		 System.out.println(db);
		 keyValueStore = openKeyValueStore(db);
	}

    @NotNull
    @Contract(" -> new")
    public SnowFlakeStoreManager openStorageManager() throws BackendException {
        return new SnowFlakeStoreManager(SnowFlakeDBStorageSetup.getSnowFlakeDBConfiguration());
    }

//    @NotNull
//    @Contract("_ -> new")
    private SnowFlakeKeyValueStore openKeyValueStore(SnowFlakeDatabase db) throws BackendException {
	    return new SnowFlakeKeyValueStore(db, new SnowFlakeStoreManager(SnowFlakeDBStorageSetup.getSnowFlakeDBConfiguration()));
    }

    public void testSFGetSlice() {

    }

    public void testDelete() {}

    public void testContainsKey() {}

    public void testGetSlice() {}

    public void testClose() {}

	@Test
    public void testinsert() throws Exception
    {
        ArrayList<HashMap<String, String>> keyAndValue = getkeyAndValue();
        String keyEncoded = keyAndValue.get(0).get("key");
        String valueEncoded = keyAndValue.get(0).get("value");

        // Decode the encoded String from Base64 to Byte[]
        byte[] keyByte = Base64.getDecoder().decode(keyEncoded);
        byte[] valueByte = Base64.getDecoder().decode(valueEncoded);

        // Convert Byte Array to StaticBuffer
        StaticBuffer key = getBuffer(keyByte);
        StaticBuffer value = getBuffer(valueByte);

        System.out.println(LogUtils.stringFormatter("Key (query) %s. Key byte (query) %s being inserted", key.toString(), keyByte.toString()));

        // Insert Static Buffers
        keyValueStore.insert(key, value, txh);

        // Get the key value now
        System.out.println(LogUtils.stringFormatter("Key %s being queries using get on tx %s", key.toString(), txh.toString()));
   	    StaticBuffer keyActual = keyValueStore.get(key, txh, "KEY");
   	    // Currently it returns only StaticBuffer for key. Will need to make it more generic
        assertEquals(key, keyActual);
        assertNotNull(keyActual);

        System.out.println("------======================---------------");
        System.out.println(key.toString());
        System.out.println(keyActual.toString());
        System.out.println("------======================---------------");

    }

    @Test
    public void testgetnew() throws Exception
    {
        ArrayList<HashMap<String, String>> keyAndValue = getkeyAndValue();
        String keyEncoded = keyAndValue.get(0).get("key");
        String valueEncoded = keyAndValue.get(0).get("value");

        // Decode the encoded String from Base64 to Byte[]
        byte[] keyByte = Base64.getDecoder().decode(keyEncoded);
        byte[] valueByte = Base64.getDecoder().decode(valueEncoded);

        // Generate StaticBuffer from byte array
        StaticBuffer key = getBuffer(keyByte);
        StaticBuffer value = getBuffer(valueByte);

        System.out.println("print StaticBuffer decoded from SnowFlake tbl");
        System.out.println("KEY=" + key.toString());
        System.out.println("VALUE=" + value.toString());

        System.out.println("Calling SnowFlake get method with StaticBuffer");
        // Do DB Get Operation on SnowFlakeKeyValueStore using Static Buffer
        StaticBuffer valueActual = keyValueStore.get(key, txh);

        System.out.println("print StaticBuffer fetched from SnowFlake after required decoding");
        System.out.println(valueActual.toString());

        // Assert
        assertNotNull(valueActual);
        assertEquals(valueActual, value);

    }

	@Test
    public void testget() throws Exception
    {
   	 String key = getkey();
   	 System.out.println(key);
   	 // Convert the Base64 encoded key to byte array using default method
     byte[] b = key.getBytes();
     // Convert the Base64 encoded key to byte array using Base64 class utilities
     byte[] foundData = Base64.getDecoder().decode(key);

     System.out.println("------- Printing diff between two ways of decoding base64 into byte[] -------------");
     System.out.println(foundData);
     System.out.println(b);
     System.out.println("------------------------------------------------------------------------------------");

     // Generate StaticBuffer from byte array
     StaticBuffer s = getBuffer(foundData);

     System.out.println("print StaticBuffer decoded from SnowFlake tbl");
     System.out.println(s.toString());

     System.out.println("Calling SnowFlake get method with StaticBuffer");
     // Do DB Get Operation on SnowFlakeKeyValueStore using Static Buffer
	 StaticBuffer sb = keyValueStore.get(s, txh, "KEY");

	 System.out.println("print StaticBuffer fetched from SnowFlake after required decoding");
	 System.out.println(sb.toString());

	 // Assert
	 assertNotNull(sb);
   	 assertEquals(s, sb);

    }

	 private static StaticBuffer getBuffer(byte[] entry) {
	        return new StaticArrayBuffer(entry);
	    }

	public Connection getConnection() throws SQLException
	{
	        Properties properties = new Properties();
	        properties.put("user", "Debasish");        // replace "" with your user name
	        properties.put("password", "Nano789!");    // replace "" with your password
	        properties.put("warehouse", "COMPUTE_WH");   // replace "" with target warehouse name
	        properties.put("db", "GRAPHDBTEST");          // replace "" with target database name
	        properties.put("schema", "GRAPHDBTEST1");      // replace "" with target schema name
	        //properties.put("tracing", "on"); // optional tracing property

	        // Replace <account> with your account, as provided by Snowflake.
	        // Replace <region_id> with the name of the region where your account is located.
	        // If your platform is AWS and your region ID is US West, you can omit the region ID segment.
	        // Replace <platform> with your platform, for example "azure".
	        // If your platform is AWS, you may omit the platform.
	        // Note that if you omit the region ID or the platform, you should also omit the
	        // corresponding "."  E.g. if your platform is AWS and your region is US West, then your
	        // connectStr will look similar to:
	        // "jdbc:snowflake://xy12345.snowflakecomputing.com";
	        String connectStr = "jdbc:snowflake://ik55883.east-us-2.azure.snowflakecomputing.com";
	        return DriverManager.getConnection(connectStr, properties);
	}

   public String getkey() throws Exception
   {
	   System.out.println("Create JDBC connection");
       Connection connection = getConnection();
       System.out.println("Done creating JDBC connection\n");

       // create statement
       System.out.println("Create JDBC statement");
       Statement statement = connection.createStatement();
       System.out.println("Done creating JDBC statement\n");

       System.out.println("Query demo");
       ResultSet resultSet = statement.executeQuery("select to_varchar(KEY, 'BASE64') from system_properties limit 1;");
       System.out.println("Metadata:");
       System.out.println("================================");

       // fetch metadata
       //System.out.println("\nData:");
       System.out.println("================================");
       String key = null;
       //int rowIdx = 0;
       while (resultSet.next()) {
          /* System.out.println("row " + rowIdx + ", column 0: " +
                   resultSet.getString(1));*/

    	   key = resultSet.getString(1);


       }
       resultSet.close();
       statement.close();
       connection.close();
       return key;
   }

    public ArrayList<HashMap<String, String>> getkeyAndValue() throws Exception
    {
        System.out.println("Create JDBC connection");
        Connection connection = getConnection();
        System.out.println("Done creating JDBC connection\n");

        // create statement
        System.out.println("Create JDBC statement");
        Statement statement = connection.createStatement();
        System.out.println("Done creating JDBC statement\n");

        System.out.println("Query demo");
        ResultSet resultSet = statement.executeQuery("select to_varchar(KEY, 'BASE64') as KEY, to_varchar(VALUE, 'BASE64') as VALUE from system_properties limit 1;");
        System.out.println("Metadata:");
        System.out.println("================================");

        // fetch metadata
        //System.out.println("\nData:");
        System.out.println("================================");
        String key = null;
        String value = null;
        //int rowIdx = 0;
        while (resultSet.next()) {
          /* System.out.println("row " + rowIdx + ", column 0: " +
                   resultSet.getString(1));*/

            key = resultSet.getString("KEY");
            value = resultSet.getString("VALUE");


        }
        resultSet.close();
        statement.close();
        connection.close();

        ArrayList result = new ArrayList();
        HashMap row = new HashMap();
        row.put("key", key);
        row.put("value", value);
        result.add(row);
        return result;
    }

    public SnowFlakeDatabase openDatabase() throws PermanentBackendException {
	    try {
            SnowFlakeConnection conn = txh.getConnection();

            return new SnowFlakeDatabase(tableName, conn);
        } catch (IOException | SnowFlakeConnectionException e) {
	        throw new PermanentBackendException(e);
        }
    }

    public SnowFlakeDatabase openDatabase(String table) throws PermanentBackendException {
        try {
            SnowFlakeConnection conn = txh.getConnection();

            return new SnowFlakeDatabase(table, conn);
        } catch (IOException | SnowFlakeConnectionException e) {
            throw new PermanentBackendException(e);
        }
    }
}
