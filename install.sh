#!/usr/bin/env bash

git pull > /dev/null 2>&1

echo "Pulled codebase from Git"

mvn clean compile package -DskipTests -Drat.skip=true -q

echo "Package build success"

/bin/cp -rf janusgraph-core/target/janusgraph-core-0.3.2.jar .
/bin/cp -rf janusgraph-snowflake/target/janusgraph-snowflake-0.3.2.jar .

echo "Build package jars for janusgraph-core and janusgraph-snowflake"

/bin/cp -rf *.jar ../janusgraph-0.3.2/janusgraph-0.3.2-hadoop2/lib/

echo "Copied jars to JanusGraph dist for testing"

echo "DONE"
