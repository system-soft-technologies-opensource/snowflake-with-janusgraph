package com.sstech.janusgraph.snowflake.snowflakedb;

import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Base64;

public class DatabaseEntry implements Comparable<DatabaseEntry>, Serializable {
    private String KEY="";
    private String VALUE="";
    private String COLUMN="";

    private byte[] keyByte;
    private byte[] valueByte;
    private byte[] columnByte;

    private String tableName = null;
    private SnowFlakeConnection conn;

    public DatabaseEntry(String key, String value) {
        KEY = key;
        VALUE = value;
        COLUMN = "";

        encodeString();
    }

    public DatabaseEntry(String key, String value, String column) {
        KEY = key;
        VALUE = value;
        COLUMN = column;

        encodeString();
    }

    public DatabaseEntry(String ... columns) {
        KEY = columns[0];
        VALUE = columns[1];
//        System.out.println(columns[2] == null);
        if (columns.length == 3 && columns[2] != null) {
            COLUMN = columns[2];
        } else {
            COLUMN = "";
        }
//        System.out.println(String.format("After assignment, vargs size %s Key: %s Val: %s Col: %s", String.valueOf(columns.length), KEY, VALUE, COLUMN));
        encodeString();
    }

    public DatabaseEntry(String tableName, SnowFlakeConnection connection, String ... columns) {
        this(columns);

        this.tableName = tableName;
        conn = connection;
    }

    private void setTableName(String table) {
        tableName = table;
    }

    private void encodeString() {
//        System.out.println("Decoding KEY = " + KEY);
        keyByte = decodeFromBase64(KEY);
//        System.out.println("Decoding VALUE = " + VALUE);
        valueByte = decodeFromBase64(VALUE);
//        System.out.println("Decoding COLUMN = " + COLUMN);
        columnByte = decodeFromBase64(COLUMN);
    }

    private byte[] decodeFromBase64(String encodedString) {
//        System.out.println("String to decode is " + encodedString);
        return Base64.getDecoder().decode(encodedString);
    }

    private String encodeToBase64(byte[] valueToEncode) {
        return Base64.getEncoder().encodeToString(valueToEncode);
    }

    public String getKeyString() {
        return KEY;
    }

    public String getValueString() {
        return VALUE;
    }

    public String getColumnString() {
        return COLUMN;
    }

    public byte[] getKey() {
        return keyByte;
    }

    public byte[] getValue() {
        return valueByte;
    }

    public byte[] getColumn() {
        return columnByte;
    }

    public StaticBuffer getKeyBuffer() {
        return getBuffer(keyByte);
    }

    public StaticBuffer getValueBuffer() {
        return getBuffer(valueByte);
    }

    public KeyValueEntry getKeyValueEntry() {
        return new KeyValueEntry(getKeyBuffer(), getValueBuffer());
    }

    public StaticBuffer getColumnBuffer() {
        return getBuffer(columnByte);
    }

    public void updateKey(String keyToUpdate) {
        KEY = keyToUpdate;
    }

    public StaticBuffer getBuffer(String columnName) {

        try {
            String encodedValue = (String) this.getClass().getField(columnName).get(this);
            byte[] bufferValue = decodeFromBase64(encodedValue);
            return getBuffer(bufferValue);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void commit() throws SQLException {
        if (tableName == null) {
            throw new SQLException("Table name is not set, This instance is local instance so this tx can't be persisted.");
        }

        String commitSQL = String.format("UPDATE %s SET KEY=to_binary(%s, 'BASE64'), VALUE=to_binary(%s, 'BASE64'), " +
            "COLUMN1=to_binary(%s, 'BASE64') WHERE KEY=to_binary(%s, 'BASE64')", tableName, getKeyString(),
            getValueString(), getColumnString(), getKeyString());

        OperationStatus res = conn.executeUpdate(commitSQL);
        int rowsUpdated = (int) res.getResult();

        System.out.println("Updated " + rowsUpdated + " rows");
    }

    public void close() {
        if (conn != null)
            conn.close();
    }

    private StaticBuffer getBuffer(byte[] entry) {
        return new StaticArrayBuffer(entry);
    }

    @Override
    public int compareTo(DatabaseEntry o) {
        return 0;
    }
}
