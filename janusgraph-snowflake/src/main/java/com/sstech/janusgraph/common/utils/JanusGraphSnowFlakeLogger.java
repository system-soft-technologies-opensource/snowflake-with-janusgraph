package com.sstech.janusgraph.common.utils;

//import com.sstech.janusgraph.common.utils.OSUtils;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class JanusGraphSnowFlakeLogger {
    private Logger logger;
    private FileHandler fh;
    private SimpleDateFormat format = new SimpleDateFormat("M-d_HHmm");

    public String logFname = null;

    public JanusGraphSnowFlakeLogger(String logFname) {
        this.logFname = logFname;
        createRequiredDirectories(this.logFname);
        logger = Logger.getLogger("JanusGraphSnowFlakeLogger");
    }

    public JanusGraphSnowFlakeLogger(String logFname, Class className) {
        this.logFname = logFname;
        logger = Logger.getLogger(className.getName());
    }

    public JanusGraphSnowFlakeLogger(Class className) {
        this.logFname = buildLogFileName();
        createRequiredDirectories(this.logFname);
        logger = Logger.getLogger(className.getName());
    }

    private String buildLogFileName() {
        boolean isUnix = OSUtils.isUnix();
        boolean isWindows = OSUtils.isWindows();
        boolean isMac = OSUtils.isMac();

        String fileName = null;

        if (isUnix || isMac) {
            fileName = "/var/log/janusgraph-snowflake/JanusSnow_" + format.format(Calendar.getInstance().getTime()) + ".log";
        }
        else if (isWindows) {
            fileName = "C:/temp/janusgraph-snowflake/JanusSnow_" + format.format(Calendar.getInstance().getTime()) + ".log";
        }

        return fileName;
    }

    public String getLogFileName() {
        return buildLogFileName();
    }

    private Boolean createRequiredDirectories(String fullFileName) {
        File f = new File(fullFileName);

        return new File(f.getParent()).mkdirs();
    }

    public Logger getLogger() throws IOException {
        if (this.logFname != null) {
            fh = new FileHandler(this.logFname, true);
            logger.addHandler(fh);
            logger.setUseParentHandlers(false);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            return logger;
        }
        else
            throw new IOException("Please specify the log input file name by calling constructor");
    }
}
