package com.sstech.janusgraph.common.utils;

import java.io.File;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class SingleFileLogger {
    private static SingleFileLogger instance = null;
    private static Logger logger;
    private static FileHandler fh;
    private static Formatter sf;
    private String logFileName;

    private SingleFileLogger(String fileName) {
        logFileName = fileName;

        createRequiredDirectories(logFileName);

        try {
            fh = new FileHandler(fileName, true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger = Logger.getLogger("LogMe");

        sf = new SimpleFormatter();
        fh.setFormatter(sf);
        logger.addHandler(fh);
        logger.setUseParentHandlers(false);
        //Part of making this class a singleton
        instance = this;
    }

    private Boolean createRequiredDirectories(String fullFileName) {
        File f = new File(fullFileName);

        return new File(f.getParent()).mkdirs();
    }

    public String getLogFileName() {
        return logFileName;
    }

    public static SingleFileLogger getInstance(String filename) {
        if (instance == null)
            instance = new SingleFileLogger(filename);

        return instance;
    }

    public static SingleFileLogger getInstance() {
        String filename = generateLogFileName();
        if (instance == null)
            instance = new SingleFileLogger(filename);

        return instance;
    }

    private static String generateLogFileName() {
        if (OSUtils.isWindows()){
            return "../logs/janusgraph-snowflake/jg-sf.log";
        }
        else {
            return "/tmp/log/janusgraph-snowflake/janusgraph-snowflake.log";
        }
    }

    public Logger getLogger() {
        return SingleFileLogger.logger;
    }
}
