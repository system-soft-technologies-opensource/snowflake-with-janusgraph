package com.sstech.janusgraph.common.comparision;

import com.sstech.janusgraph.common.comparision.datafetcher.BerkleyJEDataFetcher;
import com.sstech.janusgraph.common.comparision.datafetcher.SnowFlakeDataFetcher;
import com.sstech.janusgraph.common.storage.BerkeleyDBStorageSetup;
import com.sstech.janusgraph.common.utils.DataLoader;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.*;


public class TestSnowFlakeRetrieval {
    /*
    This Class takes into consideration that already a BerkleyDB Env(JanusGraph) is defined and exists at the Path specified.
    Is then initialized BerkleyDBReader class which reads in data from the table specified. It fetches top 20 records from edgestore DB in BDB

    We then follow it up, by making use of custom SnowFlakeDataLoader class which loads the list of records (KeyValueEntry)
    into SnowFlake DB by making use of store.insert(SB key, SB value).
    TODO: We need to do this process for all the tables once GraphOfGods is loaded successfully and do g.V().count() for checking.

    NOTE: The data loading step is done once only. Hence it is commented out in this part of code as data loading is complete.

    Once data is loaded, we then do Read Operation using store.get() to check for data consistency.

    The following assertions are done:
    1: For each BerkleyDB record fetched, in step 1, we generate KeyValueEntry and query using the same on SnowFlakeDB.
        As assertion on record fetched against SnowFlake and initial BerkleyDB is done.
    2: For each BerkleyDB record fetched, in step 1, we generate KeyValueEntry and query using the same on SnowFlakeDB.
        As assertion on keys fetched against SnowFlake and initial BerkleyDB is done.
    3: For each BerkleyDB record fetched, in step 1, we generate KeyValueEntry and query using the same on SnowFlakeDB.
        As assertion on values fetched against SnowFlake and initial BerkleyDB is done.
    */

    private String graphPath;
    private BerkleyJEDataFetcher dataFetcher;
    private HashMap<String, BerkleyJEDataFetcher> dataFetcherMap = new HashMap<>();
    private HashMap<String, DataLoader> dataLoaderMap = new HashMap<>();
    private SnowFlakeDataFetcher sfDataFetcher;
    // recordLimit = -1. when you want to fetch all records. The iteration is continued till all records are iterated over.
    private Integer recordLimit = -1;

    private List<String> tables = Arrays.asList("edgestore_lock_", "txlog", "edgestore", "graphindex_lock_",
        "systemlog", "system_properties_lock_", "janusgraph_ids", "graphindex", "system_properties");

//    private List<String> tables = Arrays.asList("edgestore");

    private List<KeyValueEntry> bdbRecords;
    private List<KeyValueEntry> edgeStoreRecords;

    private static final StaticBuffer.Factory<byte[]> ENTRY_FACTORY = (array, offset, limit) -> {
        final byte[] bArray = new byte[limit - offset];
        System.arraycopy(array, offset, bArray, 0, limit - offset);
        return bArray;
    };

//    public static void main(String ... args) throws Exception {
//        System.out.println("Starting data migration from BerkleyDB to SnowFlake for all tables");
//        Long start = System.currentTimeMillis();
//
//        TestSnowFlakeRetrieval instance = new TestSnowFlakeRetrieval();
//        instance.loadData();
//
//        System.out.println("Data loading complete");
//        instance.closeResources();
//        System.out.println("Took " + (System.currentTimeMillis() - start)/1000/60 + " minutes for data load");
//    }

    public TestSnowFlakeRetrieval() throws PermanentBackendException {

    }

    @Before
    public void loadData() throws Exception {
        System.out.println("Im setting up initial resources");

        graphPath = BerkeleyDBStorageSetup.getDataPath();
        initializeBDBDataReader();
        initializeSnowFlakeDataReader();
        dataLoaderForSnowFlake();
    }

    private void dataLoaderForSnowFlake() throws Exception {
        for (String table : tables) {
            bdbRecords = loadBerkleyDBRecords(table);

            DataLoader loader = new DataLoader(table);
            loader.load(bdbRecords);

            dataLoaderMap.put(table, loader);

            if (table.equals("edgestore")) {
                // Because ouw snowflakedatafetcher is currently hardcoded to fetch data from table edgestore so
                // we will use that table for tests
                edgeStoreRecords = bdbRecords;
            }
        }
    }

    private void initializeBDBDataReader() throws PermanentBackendException {
        for (String table : tables) {
            dataFetcher = new BerkleyJEDataFetcher(graphPath);
            dataFetcher.initializeDataFetchEngine(table);
            dataFetcherMap.put(table, dataFetcher);
        }
    }

    private void initializeSnowFlakeDataReader() throws Exception {
        sfDataFetcher = new SnowFlakeDataFetcher();
        sfDataFetcher.initializeDataFetchEngine();
    }

    private KeyValueEntry getRecord(KeyValueEntry entry) throws SQLException {
        StaticBuffer key = entry.getKey();
        StaticBuffer value = entry.getValue();

        byte[] keyByte = key.as(ENTRY_FACTORY);
        byte[] valueByte = value.as(ENTRY_FACTORY);

        return sfDataFetcher.getKeyValueEntry(keyByte, valueByte);
    }

    private KeyValueEntry getRecord(StaticBuffer key) throws SQLException {
        byte[] keyByte = key.as(ENTRY_FACTORY);

        return sfDataFetcher.getMatchingKeyValueEntries(keyByte);
    }

    private List<KeyValueEntry> loadBerkleyDBRecords() {
        List<KeyValueEntry> records = dataFetcher.getTopKeyValueEntries(recordLimit);
        return records;
    }

    private List<KeyValueEntry> loadBerkleyDBRecords(String table) {
        List<KeyValueEntry> records = dataFetcherMap.get(table).getTopKeyValueEntries(recordLimit);
        System.out.println("Total records fetched from BerkleyDB for table " + table + " is " + records.size());
        return records;
    }

    @Test
    public void testConsistentDataAcrossSources() throws SQLException {
        // This Test tests for data consistency across DBD and SnowFlake
        // Reads in data from edgestore table in DDB
        // Inserts data into SnowFlake after extracting from BDB
        // Then fetches from SnowFlake using KeyValue Entry and does an assertion

        // Considers that the first two steps are done. i.e. Loading from BDB and Loading in SF is done.

        List<KeyValueEntry> sfRecords = new ArrayList<>();

        edgeStoreRecords.forEach( record -> {
            KeyValueEntry sfRecord = null;
            try {
                sfRecord = getRecord(record);
//                System.out.println(sfRecord);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            sfRecords.add(sfRecord);
        });

        System.out.println(edgeStoreRecords.size() + " and " + sfRecords.size());

        for (int i = 0; i < edgeStoreRecords.size(); i++) {
            KeyValueEntry bdbRecord = edgeStoreRecords.get(i);
            KeyValueEntry sfRecord = sfRecords.get(i);

//            System.out.println(bdbRecord);
//            System.out.println(sfRecord);

            Assert.assertEquals(bdbRecord.getKey(), sfRecord.getKey());
            Assert.assertEquals(bdbRecord.getValue(), sfRecord.getValue());
        }
    }

    @Test
    public void testConsistentKeyAcrossSources() {
        List<KeyValueEntry> sfRecords = new ArrayList<KeyValueEntry>();

        edgeStoreRecords.forEach( record -> {
            KeyValueEntry sfRecord = null;
            try {
                sfRecord = getRecord(record.getKey());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            sfRecords.add(sfRecord);
        });

        for (int i = 0; i < edgeStoreRecords.size(); i++) {
            KeyValueEntry bdbRecord = edgeStoreRecords.get(i);
            KeyValueEntry sfRecord = sfRecords.get(i);

            Assert.assertEquals(bdbRecord.getKey(), sfRecord.getKey());
        }
    }

    @Test
    public void testConsistentValueAcrossSources() {
        List<KeyValueEntry> sfRecords = new ArrayList<KeyValueEntry>();

        edgeStoreRecords.forEach( record -> {
            KeyValueEntry sfRecord = null;
            try {
                sfRecord = getRecord(record.getKey());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            sfRecords.add(sfRecord);
        });

        for (int i = 0; i < edgeStoreRecords.size(); i++) {
            KeyValueEntry bdbRecord = edgeStoreRecords.get(i);
            KeyValueEntry sfRecord = sfRecords.get(i);

            Assert.assertEquals(bdbRecord.getValue(), sfRecord.getValue());
        }
    }

    @After
    public void closeResources() throws Exception {
        if (dataFetcher != null) {
            dataFetcher.closeResources();
        }
        if (sfDataFetcher != null) {
            sfDataFetcher.closeResources();
        }
        for (Map.Entry<String, DataLoader> entry : dataLoaderMap.entrySet()) {
            DataLoader loader = entry.getValue();
            loader.close();
        }
    }
}
