package com.sstech.janusgraph.diskstorage.snowflake;

import com.sstech.janusgraph.common.comparision.TestBerkleyJERetrieval;
import com.sstech.janusgraph.common.comparision.TestRetrievalComparision;
import com.sstech.janusgraph.common.comparision.TestSnowFlakeRetrieval;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    TestBerkleyJERetrieval.class,
    TestSnowFlakeRetrieval.class,
    TestRetrievalComparision.class,
    TestSnowFlakeKeyValueStore.class,
    SnowFlakeKeyValueTest.class,
    SnowFlakeVariableLengthKCVSTest.class})

public class SnowFlakeTestSuite {

}
