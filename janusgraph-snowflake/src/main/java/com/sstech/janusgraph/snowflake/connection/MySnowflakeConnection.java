package com.sstech.janusgraph.snowflake.connection;

import net.snowflake.client.jdbc.SnowflakeConnectionV1;

import java.sql.SQLException;
import java.util.Properties;

public class MySnowflakeConnection extends SnowflakeConnectionV1 {

    public MySnowflakeConnection(String url, Properties info) throws SQLException {
        super(url, info);
    }
}
