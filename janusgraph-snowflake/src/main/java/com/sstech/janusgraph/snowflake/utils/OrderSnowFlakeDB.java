package com.sstech.janusgraph.snowflake.utils;

import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;

import java.sql.SQLException;

public class OrderSnowFlakeDB {
    private String table;
    private SnowFlakeConnection conn;

    private String orderBy = "KEY";
    private String tempTableName;

    public OrderSnowFlakeDB(String tableName, SnowFlakeConnection connection) {
        table = tableName;
        conn = connection;
        tempTableName = String.format("temp_%s", table);
    }

    private OperationStatus prepareCreateTable() {
        String sql ="CREATE TABLE " + tempTableName + " IF NOT EXISTS (KEY BINARY, VALUE BINARY, COLUMN1 BINARY)";
        return conn.executeUpdate(sql);
    }

    private OperationStatus copyOrderedDataFromSource() {
        String sql = String.format("INSERT INTO %s (KEY, VALUE) SELECT KEY, VALUE FROM %s ORDER BY %s",
            tempTableName, table, orderBy);

        return conn.executeUpdate(sql);
    }

    private OperationStatus backupOriginalTable() {
        String sql = String.format("ALTER TABLE %s RENAME TO %s1", table, table);
        return conn.executeUpdate(sql);
    }

    private OperationStatus renameTemporaryTable() {
        String sql = String.format("ALTER TABLE %s RENAME TO %s", tempTableName, table);
        return conn.executeUpdate(sql);
    }

    private OperationStatus dropBackupTables() {
        String sql = String.format("DROP TABLE IF EXISTS %s1", table);
        conn.executeUpdate(sql);
        sql = String.format("DROP TABLE IF EXISTS %s", tempTableName);
        return conn.executeUpdate(sql);
    }

    public boolean order() throws SQLException {
        boolean status;
        OperationStatus result;

        result = prepareCreateTable();
        status = result.getStatus();
        if (!status)
            throw new SQLException("Unable to create temporary table " + tempTableName);

        result = copyOrderedDataFromSource();
        status = result.getStatus();
        if (!status)
            throw new SQLException("Unable to copy order data from src to target");

        result = backupOriginalTable();
        status = result.getStatus();
        if (!status)
            throw new SQLException("Unable to backup original table to " + table + "1");

        result = renameTemporaryTable();
        status = result.getStatus();
        if (!status)
            throw new SQLException("Unable to rename " + tempTableName + " to " + table);

        result = dropBackupTables();
        status = result.getStatus();
        if (!status)
            throw new SQLException("Unable to drop backup tables " + table + "1 and " + tempTableName);

        return true;
    }
}
