package com.sstech.janusgraph.snowflake.snowflakedb;

import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.common.utils.SingleFileLogger;
import net.snowflake.client.jdbc.SnowflakeSQLException;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

//import com.sstech.janusgraph.common.utils.LogMe;

public class SnowFlakeCreateStatement implements Serializable {

    private Connection connection;
    private Statement statement;

//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeCreateStatement.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
    private SingleFileLogger logger = SingleFileLogger.getInstance();
    private Logger log = logger.getLogger();

    public SnowFlakeCreateStatement(Connection snowFlakeConnection) throws SQLException, IOException {
//        log = logger.getLogger();
        connection = snowFlakeConnection;
        statement = connection.createStatement();
//        try {
//            log = logger.getLogger();
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new IOException(LogUtils.stringFormatter("Unable to create log file %s while executing", logger.getLogFileName()));
//        }

    }

    public int executeUpdate(String sql) throws SQLException {
        log.info(LogUtils.stringFormatter("Successfully executed update %s from cursor", sql));
        return statement.executeUpdate(sql);
    }

    public Statement getStatement() {
        return statement;
    }

    public ResultSet executeQuery(String sql) throws SQLException {
//        ResultSet result = null;
        log.info("Executing SQL query " + sql + " from cursor");
        try {
//            return connection.prepareStatement(sql,
//                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery();
//            System.out.println("Statement status " + statement.isClosed());
            return statement.executeQuery(sql);
        } catch (SnowflakeSQLException e) {
            System.out.println("Exception raised while executing query " + sql);
            System.out.println(statement.isClosed());
            e.printStackTrace();
//            System.exit(-1);
            throw new SQLException("Unable to run the query " + sql + " Is statement null? " + String.valueOf(statement == null) + String.valueOf(connection == null));
        }
//        try{
//            ResultSet result = statement.executeQuery(sql);
//            log.info(LogUtils.stringFormatter("Successfully executed query %s from cursor", sql));
//            return result;
//        }
//        catch (SnowflakeSQLException e) {
//            System.out.println("SnowFlakeSQL Exception in running query " + sql);
//            log.severe("Unable to run query " + sql + " from cursor");
//            throw new SQLException("Unable to run the query " + sql + " Is statement null? " + String.valueOf(statement == null) + String.valueOf(connection == null));
//        }
    }

    public void close() throws SQLException {
        try {
            statement.close();
//            System.out.println("Closed snowflake statement from createsttement");
            log.info("Successfully closed connection for statement object in cursor");
            connection.close();
//            System.out.println("Closed snowflake connection from createsttement");
            log.info("Successfully closed connection for JDBC connection object in cursor");
        }
        catch (SQLException e) {
            log.severe("Unable to close connection to SnowFlake from cursor");
        }
    }
}
