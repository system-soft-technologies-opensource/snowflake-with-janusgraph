package com.sstech.janusgraph.common.utils;

import org.apache.commons.lang.StringUtils;

import java.io.PrintWriter;
import java.io.StringWriter;

public final class LogUtils {

    public static String logTraceback(String msg, Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);

        String fullMsg = msg + sw.toString();

        return fullMsg;
    }

    public static String stringFormatter(String msg, String ... args) {
        if (args.length == 0) {
            return msg;
        }
        else {
            int matches = 0;
            if (msg.contains("{}")) {
                matches = StringUtils.countMatches(msg, "{}");
            }
            else {
                matches = StringUtils.countMatches(msg, "%s");
            }

            if (matches == args.length) {
                if (msg.contains("{}"))
                    return String.format(msg.replace("{}", "%s"), args);
                else
                    return String.format(msg, args);
            }
            else {
                throw new IllegalArgumentException("Number of %s in parent string need to match with " +
                        "number of passed substrings");
            }
        }
    }
}
