package com.sstech.janusgraph.diskstorage.snowflake;

import org.janusgraph.diskstorage.configuration.ConfigNamespace;
import org.janusgraph.diskstorage.configuration.ConfigOption;
import org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration;
import org.janusgraph.graphdb.configuration.PreInitializeConfigOptions;


@PreInitializeConfigOptions
public interface SnowFlakeDBConfigOptions {
    ConfigNamespace SF_NS = new ConfigNamespace(
        GraphDatabaseConfiguration.STORAGE_NS,
        "sf",
        "SnowFlake DB storage backend options");

    ConfigOption<String> GRAPH = new ConfigOption<>(
        SF_NS,
        "DB",
        "The name of the JanusGraph Graph in SnowFlake DB.  It will be created if it does not exist.",
        ConfigOption.Type.LOCAL,
        "janusgraph");

    ConfigOption<String> SCHEMA = new ConfigOption<>(
        SF_NS,
        "SCHEMA",
        "The name of the JanusGraph Graph in SnowFlake DB.  It will be created if it does not exist.",
        ConfigOption.Type.LOCAL,
        "janusgraph");

    ConfigOption<String> VERSION = new ConfigOption<>(
        SF_NS,
        "version",
        "The version of the SnowFlake DB cluster.",
        ConfigOption.Type.LOCAL,
        "3.10.3");

    ConfigOption<String> CLUSTER_FILE_PATH = new ConfigOption<>(
        SF_NS,
        "cluster-file-path",
        "Path to the SnowFlake DB cluster file",
        ConfigOption.Type.LOCAL,
        String.class);

}
