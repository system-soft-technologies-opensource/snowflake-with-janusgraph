// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package com.sstech.janusgraph.diskstorage.snowflake;


import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.GraphUtils;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.common.utils.SingleFileLogger;
import com.sstech.janusgraph.snowflake.snowflakedb.DatabaseEntry;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeCursor;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.Entry;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.StoreTransaction;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.*;
import org.janusgraph.diskstorage.util.RecordIterator;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

public class SnowFlakeKeyValueStore implements OrderedKeyValueStore{

//    private static final Logger log = LoggerFactory.getLogger(SnowFlakeKeyValueStore.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeKeyValueStore.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
//    private SingletonLogger logger = SingletonLogger.getInstance("/var/log/custom.log");

    /*
    TODO
    TODO
    TODO: Insert needs to be done on sorted basis. So first we need to query to find the perfect place for incoming KEY
    TODO: and then use the index to insert the data inside table.
    TODO
    TODO
     */

    private static SingleFileLogger logger = SingleFileLogger.getInstance();
    private Logger log = logger.getLogger();
    private boolean isLogged = true;

    private final String name;
    private SnowFlakeStoreManager storeManager;
    private SnowFlakeDatabase db;
    private boolean isOpen;

    private String PARENT_QUERY_ID = null;

    private static final StaticBuffer.Factory<byte[]> ENTRY_FACTORY = (array, offset, limit) -> {
        final byte[] bArray = new byte[limit - offset];
        System.arraycopy(array, offset, bArray, 0, limit - offset);
        return bArray;
    };

    public SnowFlakeKeyValueStore(SnowFlakeDatabase database, SnowFlakeStoreManager manager) {

        name = database.getTableName();
        db = database;
        storeManager = manager;
        isOpen = true;

        PARENT_QUERY_ID = getParentQueryID();
    }

    private String getParentQueryID() {
        if (PARENT_QUERY_ID == null)
            return UUID.randomUUID().toString();
        else
            return PARENT_QUERY_ID;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void insert(StaticBuffer key, StaticBuffer value, StoreTransaction txh) throws BackendException {
        insert(key, value, txh, true);
    }

    public void insert(StaticBuffer key, StaticBuffer value, StoreTransaction txh, boolean allowOverwrite) throws BackendException {

        if (SnowFlakeStoreManager.SKIP_LOG_TABLES) {
            if (GraphUtils.TABLES_TO_OPERATE_ON.contains(db.getTableName())) {
                try {
                    OperationStatus status;

                    if (isLogged) {
                        log.info(String.format("db=%s, op=insert, tx=%s", name, txh));
                    }

                    byte[] keyByte = key.as(ENTRY_FACTORY);
                    byte[] valueByte = value.as(ENTRY_FACTORY);

                    if (allowOverwrite)
                        status = db.put(keyByte, valueByte);
                    else
                        status = db.putNotOverWrite(keyByte, valueByte);

                } catch (Exception e) {
                    throw new PermanentBackendException(e);
                }
            }
        } else {
            try {
                OperationStatus status;

                if (isLogged) {
                    log.info(String.format("db=%s, op=insert, tx=%s", name, txh));
                }

                byte[] keyByte = key.as(ENTRY_FACTORY);
                byte[] valueByte = value.as(ENTRY_FACTORY);

                if (allowOverwrite)
                    status = db.put(keyByte, valueByte);
                else
                    status = db.putNotOverWrite(keyByte, valueByte);

            } catch (Exception e) {
                throw new PermanentBackendException(e);
            }
        }
//
//        if (SnowFlakeStoreManager.SKIP_LOG_TABLES && GraphUtils.TABLES_TO_OPERATE_ON.contains(db.getTableName())) {
//            try {
//                OperationStatus status;
//
//                if (isLogged) {
//                    log.info(String.format("db=%s, op=insert, tx=%s", name, txh));
//                }
//
//                byte[] keyByte = key.as(ENTRY_FACTORY);
//                byte[] valueByte = value.as(ENTRY_FACTORY);
//
//                if (allowOverwrite)
//                    status = db.put(keyByte, valueByte);
//                else
//                    status = db.putNotOverWrite(keyByte, valueByte);
//
//            } catch (Exception e) {
//                throw new PermanentBackendException(e);
//            }
//        }
    }

//    @Override
    public void mutate(final StaticBuffer key, final List<Entry> additions, final List<StaticBuffer> deletions, final StoreTransaction txh) throws BackendException {

        List<KeyValueEntry> additionEntries = new ArrayList<>();
        additions.forEach( addition -> additionEntries.add(new KeyValueEntry(addition.getColumn(), addition.getValue())));

        Map<String, KVMutation> mutation = new HashMap<>();
        mutation.put(getName(), new KVMutation(additionEntries, deletions));
        this.storeManager.mutateMany(mutation, txh);
    }

    @Override
    public RecordIterator<KeyValueEntry> getSlice(KVQuery query, StoreTransaction txh, String localQueryId) throws BackendException {
        if (localQueryId!=null) {
            System.out.println("================");
//            System.out.println("I'm executing query with following specifications.");
//            System.out.println(String.format("Start: %s End %s Limit %s Selector %s", query.getStart(), query.getEnd(), query.getLimit(), query.getKeySelector()));

            if (localQueryId.contains("indexQuery"))
                System.out.println(String.format("Querying Index table '%s' with queryId '%s' from inside getSlice. ", name, localQueryId));
            else
                System.out.println(String.format("Querying EdgeStore table '%s' with queryId '%s' from inside getSlice. ", name, localQueryId));
        }

        if (SnowFlakeStoreManager.SKIP_LOG_TABLES) {
            if (GraphUtils.TABLES_TO_OPERATE_ON.contains(db.getTableName())) {
                if (isLogged) {
                    log.info(String.format("db=%s, op=getSlice, tx=%s", name, txh));
                }

                final StaticBuffer keyStart = query.getStart();
                final StaticBuffer keyEnd = query.getEnd();
                final KeySelector selector = query.getKeySelector();

                final List<KeyValueEntry> result = new ArrayList<>();
                final byte[] foundKey = keyStart.as(ENTRY_FACTORY);

                SnowFlakeDatabase sfdb = null;

                try {
                    sfdb = new SnowFlakeDatabase(name, storeManager.createFreshTransaction());
                } catch (SnowFlakeConnectionException | IOException e) {
                    e.printStackTrace();
                    throw new PermanentBackendException("Unable to create connection to new SnowFlakeDatabase in getSlice from store");
                }

                assert sfdb != null;
                SnowFlakeCursor cursor = sfdb.openCursor();

                List<DatabaseEntry> res;
                Map<StaticBuffer, StaticBuffer> keyValueMap = new TreeMap<>();
                Integer limit = -1;

                long start = System.currentTimeMillis();

                try {
                    res = cursor.getKeyRange(foundKey, keyEnd.as(ENTRY_FACTORY));
                } catch (SQLException e) {
                    System.exit(-1000);

                    e.printStackTrace();
                    throw new PermanentBackendException("Unable to find key " + foundKey.toString());
                }
                long end = System.currentTimeMillis();

                for (int i = 0; i < res.size(); i++) {
                    DatabaseEntry entry = res.get(i);
                    StaticBuffer key = entry.getKeyBuffer();

                    if (key.compareTo(keyEnd) >= 0) {
                        break;
                    }

                    if (selector.include(key)) {
                        StaticBuffer value = entry.getValueBuffer();
                        KeyValueEntry keyValueEntry = entry.getKeyValueEntry();

                        keyValueMap.put(key, value);

                        if (selector.reachedLimit()) {
                            if (limit == -1) {
                                limit = i;
                            }
                        }
                    }
                }

                int pos = 0;
                for (Map.Entry<StaticBuffer, StaticBuffer> entry : keyValueMap.entrySet()) {
                    StaticBuffer kvKey = entry.getKey();
                    StaticBuffer kvVal = entry.getValue();
                    KeyValueEntry kvEntry = new KeyValueEntry(kvKey, kvVal);

                    if (pos > limit && limit >= 0)
                        // Limit is 0, when the previous loop breaks in 1st iteration i.e. when specified limit = 1.
                        // We could have assigned limit = i+1 to make in standard, but that could break this if logic.
                        break;

                    result.add(kvEntry);
                    pos++;
                }

                log.info(LogUtils.stringFormatter("db={%s}, op=getSlice, tx={%s}, resultcount={%s}",
                        name, txh.toString(), String.valueOf(result.size())));

                SnowFlakeRecordIterator it = new SnowFlakeRecordIterator(result);

                cursor.close();
                sfdb.close();

                if (name.equals("edgestore") || name.equals("graphindex") || name.equals("janusgraph_ids"))
                    System.out.println("================");
                return it;
            }
            else
                return new SnowFlakeRecordIterator(new ArrayList<>());
        }
        else {
            if (isLogged) {
                log.info(String.format("db=%s, op=getSlice, tx=%s", name, txh));
            }

            final StaticBuffer keyStart = query.getStart();
            final StaticBuffer keyEnd = query.getEnd();
            final KeySelector selector = query.getKeySelector();

            final List<KeyValueEntry> result = new ArrayList<>();
            final byte[] foundKey = keyStart.as(ENTRY_FACTORY);

            SnowFlakeDatabase sfdb = null;

            try {
                sfdb = new SnowFlakeDatabase(name, storeManager.createFreshTransaction());
            } catch (SnowFlakeConnectionException | IOException e) {
                e.printStackTrace();
                throw new PermanentBackendException("Unable to create connection to new SnowFlakeDatabase in getSlice from store");
            }

            assert sfdb != null;
            SnowFlakeCursor cursor = sfdb.openCursor();

            List<DatabaseEntry> res;
            Map<StaticBuffer, StaticBuffer> keyValueMap = new TreeMap<>();
            Integer limit = -1;

            long start = System.currentTimeMillis();

            try {
                res = cursor.getKeyRange(foundKey, keyEnd.as(ENTRY_FACTORY));
            } catch (SQLException e) {
                System.exit(-1000);

                e.printStackTrace();
                throw new PermanentBackendException("Unable to find key " + foundKey.toString());
            }
            long end = System.currentTimeMillis();

            for (int i = 0; i < res.size(); i++) {
                DatabaseEntry entry = res.get(i);
                StaticBuffer key = entry.getKeyBuffer();

                if (key.compareTo(keyEnd) >= 0) {
                    break;
                }

                if (selector.include(key)) {
                    StaticBuffer value = entry.getValueBuffer();
                    KeyValueEntry keyValueEntry = entry.getKeyValueEntry();

                    keyValueMap.put(key, value);

                    if (selector.reachedLimit()) {
                        if (limit == -1) {
                            limit = i;
                        }
                    }
                }
            }

            int pos = 0;
            for (Map.Entry<StaticBuffer, StaticBuffer> entry : keyValueMap.entrySet()) {
                StaticBuffer kvKey = entry.getKey();
                StaticBuffer kvVal = entry.getValue();
                KeyValueEntry kvEntry = new KeyValueEntry(kvKey, kvVal);

                if (pos > limit && limit >= 0)
                    // Limit is 0, when the previous loop breaks in 1st iteration i.e. when specified limit = 1.
                    // We could have assigned limit = i+1 to make in standard, but that could break this if logic.
                    break;

                result.add(kvEntry);
                pos++;
            }

            log.info(LogUtils.stringFormatter("db={%s}, op=getSlice, tx={%s}, resultcount={%s}",
                    name, txh.toString(), String.valueOf(result.size())));

            SnowFlakeRecordIterator it = new SnowFlakeRecordIterator(result);

            cursor.close();
            sfdb.close();
            if (name.equals("edgestore") || name.equals("graphindex") || name.equals("janusgraph_ids"))
                System.out.println("================");
            return it;
        }
    }

    @Override
    public RecordIterator<KeyValueEntry> getSlice(KVQuery query, StoreTransaction txh) throws BackendException {
        return getSlice(query, txh, null);
//        if (SnowFlakeStoreManager.SKIP_LOG_TABLES && GraphUtils.TABLES_TO_OPERATE_ON.contains(db.getTableName())) {
//            if (isLogged) {
//                log.info(String.format("db=%s, op=getSlice, tx=%s", name, txh));
//            }
//
//            final StaticBuffer keyStart = query.getStart();
//            final StaticBuffer keyEnd = query.getEnd();
//            final KeySelector selector = query.getKeySelector();
//
//            final List<KeyValueEntry> result = new ArrayList<>();
//            final byte[] foundKey = keyStart.as(ENTRY_FACTORY);
//
//            SnowFlakeDatabase sfdb = null;
//
//            try {
//                sfdb = new SnowFlakeDatabase(name, storeManager.createFreshTransaction());
//            } catch (SnowFlakeConnectionException | IOException e) {
//                e.printStackTrace();
//                throw new PermanentBackendException("Unable to create connection to new SnowFlakeDatabase in getSlice from store");
//            }
//
//            assert sfdb != null;
//            SnowFlakeCursor cursor = sfdb.openCursor();
//
//            List<DatabaseEntry> res;
//            Map<StaticBuffer, StaticBuffer> keyValueMap = new TreeMap<>();
//            Integer limit = -1;
//
//            long start = System.currentTimeMillis();
//
//            try {
//                res = cursor.getKeyRange(foundKey, keyEnd.as(ENTRY_FACTORY));
//            } catch (SQLException e) {
//                System.exit(-1000);
//
//                e.printStackTrace();
//                throw new PermanentBackendException("Unable to find key " + foundKey.toString());
//            }
//            long end = System.currentTimeMillis();
//
//            for (int i = 0; i < res.size(); i++) {
//                DatabaseEntry entry = res.get(i);
//                StaticBuffer key = entry.getKeyBuffer();
//
//                if (key.compareTo(keyEnd) >= 0) {
//                    break;
//                }
//
//                if (selector.include(key)) {
//                    StaticBuffer value = entry.getValueBuffer();
//                    KeyValueEntry keyValueEntry = entry.getKeyValueEntry();
//
//                    keyValueMap.put(key, value);
//
//                    if (selector.reachedLimit()) {
//                        if (limit == -1) {
//                            limit = i;
//                        }
//                    }
//                }
//            }
//
//            int pos = 0;
//            for (Map.Entry<StaticBuffer, StaticBuffer> entry : keyValueMap.entrySet()) {
//                StaticBuffer kvKey = entry.getKey();
//                StaticBuffer kvVal = entry.getValue();
//                KeyValueEntry kvEntry = new KeyValueEntry(kvKey, kvVal);
//
//                if (pos > limit && limit >= 0)
//                    // Limit is 0, when the previous loop breaks in 1st iteration i.e. when specified limit = 1.
//                    // We could have assigned limit = i+1 to make in standard, but that could break this if logic.
//                    break;
//
//                result.add(kvEntry);
//                pos++;
//            }
//
//            log.info(LogUtils.stringFormatter("db={%s}, op=getSlice, tx={%s}, resultcount={%s}",
//                name, txh.toString(), String.valueOf(result.size())));
//
//            SnowFlakeRecordIterator it = new SnowFlakeRecordIterator(result);
//
//            cursor.close();
//            sfdb.close();
//
//            return it;
//        }
//        else {
//            // If the table being executed doesn't belong to permitted tables then we simple return empty iterator
//            return new SnowFlakeRecordIterator(new ArrayList<>());
//        }
    }

    public class SnowFlakeRecordIterator implements RecordIterator<KeyValueEntry> {
        private final Iterator<KeyValueEntry> entries;
        private final int len;

        public SnowFlakeRecordIterator(List<KeyValueEntry> results) {
            entries = results.iterator();
            len = results.size();
        }

        @Override
        public boolean hasNext() {
            return entries.hasNext();
        }

        @Override
        public KeyValueEntry next() {
            return entries.next();
        }

        @Override
        public void close() {
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        public int size() {
            return len;
        }
    }

    public RecordIterator<KeyValueEntry> getSliceOld(KVQuery query, StoreTransaction txh) throws BackendException {
        if (isLogged) {
            log.info(String.format("db=%s, op=getSlice, tx=%s", name, txh));
        }
//
//        try {
//            initializeDatabase((SnowFlakeTransaction) txh);
//        } catch (SnowFlakeConnectionException e ){
//            throw new PermanentBackendException(e);
//        } catch (IOException e ){
//            throw new PermanentBackendException(e);
//        }

//        if (db == null) {
//            try {
//                initializeDatabase();
//            } catch (SnowFlakeConnectionException e ){
//                throw new PermanentBackendException(e);
//            } catch (IOException e ){
//                throw new PermanentBackendException(e);
//            }
//        }
//        System.out.println("DB is " + db.getTableName());
//        final Transaction tx = getTransaction(txh);
        final StaticBuffer keyStart = query.getStart();
        final StaticBuffer keyEnd = query.getEnd();
        final KeySelector selector = query.getKeySelector();

        final List<KeyValueEntry> result = new ArrayList<>();
        final byte[] foundKey = keyStart.as(ENTRY_FACTORY);
//        final byte[] foundKey = null;
//        final DatabaseEntry foundData = new DatabaseEntry();
//        final byte[] foundData = null;
        SnowFlakeCursor cursor = db.openCursor();



//        try {
//            cursor = db.openCursor();
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new PermanentBackendException("Unable to open Cursor as Log file creation failed");
//        }

//        try (final Cursor cursor = db.openCursor(tx, null)) {
//            OperationStatus status = cursor.getSearchKeyRange(foundKey, foundData, getLockMode(txh));
//            //Iterate until given condition is satisfied or end of records
//            while (status.getStatus()) {
//                StaticBuffer key = getBuffer(foundKey);
//
//                if (key.compareTo(keyEnd) >= 0)
//                    break;
//
//                if (selector.include(key)) {
//                    result.add(new KeyValueEntry(key, getBuffer(foundData)));
//                }
//
//                if (selector.reachedLimit())
//                    break;
//
//                status = cursor.getNext(foundKey, foundData, getLockMode(txh));
//            }
//        } catch (Exception e) {
//            throw new PermanentBackendException(e);
//        }
        OperationStatus status = new OperationStatus(true);
        //Iterate until given condition is satisfied or end of records

        ResultSet res;
//        System.out.println(String.format("Searching for key %s in table %s", foundKey.toString(), cursor.toString()));
        res = cursor.getSearchKey(foundKey);

//        if (res == null) {
//            throw new PermanentBackendException("Unable to find the key " + foundKey +
//                    " and " + keyStart.toString() + " from tbl " + db.getTableName());
//        }

        try {
            while (res.next()) {

                StaticBuffer key = getBuffer(foundKey);

                if (key.compareTo(keyEnd) >= 0)
                    break;

                if (selector.include(key)) {
                    String encodedData = res.getString("VALUE");

                    byte[] foundData = Base64.getDecoder().decode(encodedData);

                    result.add(new KeyValueEntry(key, getBuffer(foundData)));
                }

                if (selector.reachedLimit())
                    break;

//            status = cursor.getNext(foundKey, foundData, getLockMode(txh));
            }
        }
        catch (SQLException e) {
            throw new PermanentBackendException("Invalid iteration over records");
        } catch (NullPointerException e) {

        }

//        log.trace("beginning db={}, op=getSlice, tx={}", name, txh);
//        final StaticBuffer keyStart = query.getStart();
//        final StaticBuffer keyEnd = query.getEnd();
//        final KeySelector selector = query.getKeySelector();
//        final List<KeyValueEntry> result = new ArrayList<>();
//        final byte[] foundKey = db.pack(keyStart.as(ENTRY_FACTORY));
//        final byte[] endKey = db.pack(keyEnd.as(ENTRY_FACTORY));
//
//        try {
//            final List<KeyValue> results = tx.getRange(foundKey, endKey, query.getLimit());
//
//            for (final KeyValue keyValue : results) {
//                StaticBuffer key = getBuffer(db.unpack(keyValue.getKey()).getBytes(0));
//                if (selector.include(key))
//                    result.add(new KeyValueEntry(key, getBuffer(keyValue.getValue())));
//            }
//        } catch (Exception e) {
//            throw new PermanentBackendException(e);
//        }
//
//        log.trace("db={}, op=getSlice, tx={}, resultcount={}", name, txh, result.size());

        if (isLogged) {
            log.info(String.format("db=%s, op=getSlice, key=%s, tx=%s, resultcount=%s", name,
                    Base64.getEncoder().encodeToString(foundKey), txh, result.size()));
        }

//        log.trace("db={}, op=getSlice, tx={}, resultcount={}", name, txh, result.size());

//        return new RecordIterator<KeyValueEntry>() {
//            private final Iterator<KeyValueEntry> entries = result.iterator();
//
//            @Override
//            public boolean hasNext() {
//                return entries.hasNext();
//            }
//
//            @Override
//            public KeyValueEntry next() {
//                return entries.next();
//            }
//
//            @Override
//            public void close() {
//            }
//
//            @Override
//            public void remove() {
//                throw new UnsupportedOperationException();
//            }
//        };
        return null;
    }

    @Override
    public Map<KVQuery,RecordIterator<KeyValueEntry>> getSlices(List<KVQuery> queries, StoreTransaction txh) throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(StaticBuffer key, StoreTransaction txh) throws BackendException {

        if (SnowFlakeStoreManager.SKIP_LOG_TABLES) {
            if (GraphUtils.TABLES_TO_OPERATE_ON.contains(db.getTableName())) {
                if (isLogged) {
                    log.info(String.format("db=%s, op=delete, tx=%s, key=%s", name, txh, key.toString()));
                }

                OperationStatus status;
                try {
                    if (isLogged) {
                        log.info(String.format("db=%s, op=deleteSuccess, tx=%s, key=%s", name, txh, key.toString()));
                    }

                    byte[] keyByte = key.as(ENTRY_FACTORY);

                    status = db.delete(keyByte);

                    if (!status.getStatus()) {
                        throw new PermanentBackendException("Could not remove: " + status);
                    }
                } catch (Exception e) {
                    throw new PermanentBackendException(e);
                }
            }
        }
        else {
            if (isLogged) {
                log.info(String.format("db=%s, op=delete, tx=%s, key=%s", name, txh, key.toString()));
            }

            OperationStatus status;
            try {
                if (isLogged) {
                    log.info(String.format("db=%s, op=deleteSuccess, tx=%s, key=%s", name, txh, key.toString()));
                }

                byte[] keyByte = key.as(ENTRY_FACTORY);

                status = db.delete(keyByte);

                if (!status.getStatus()) {
                    throw new PermanentBackendException("Could not remove: " + status);
                }
            } catch (Exception e) {
                throw new PermanentBackendException(e);
            }
        }
    }

    @Override
    public StaticBuffer get(StaticBuffer key, StoreTransaction txh) throws BackendException {
        return get(key, txh, "VALUE");
//        try {
////            DatabaseEntry databaseKey = key.as(ENTRY_FACTORY);
////            DatabaseEntry data = new DatabaseEntry();
//
//            if (isLogged) {
//                log.info(String.format("db=%s, op=get, tx=%s, key=%s", name, txh, key.toString()));
//            }
//
//            // Convert StaticBuffer to byte array
//            byte[] keyByte = key.as(ENTRY_FACTORY);
//
//            // Encode byte array to Base64.
////            String encodedkey = Base64.getEncoder().encodeToString(keyByte);
//
//            // Do DB Operation using Base64 Key.
//            OperationStatus status = db.get(keyByte);
////
//            if (status.getStatus()) {
//                // ArrayList result = status.getResultAsList("KEY", "VALUE").get(0);
//                ArrayList<String> result = status.getResultAsList("VALUE");
//                String dbEncodedString = result.get(0);
//
//                // Decode the return Base64 String to byte array
//                byte[] entry = Base64.getDecoder().decode(dbEncodedString);
//                //byte[] entry = encodedEntry.getBytes();
//
////            final byte[] entry = null;
//
//                // Convert Byte Array to StaticBuffer.
//                return getBuffer(entry);
//            }
//            else
//                return getBuffer(null);


//            // ArrayList result = status.getResultAsList("KEY", "VALUE").get(0);
//            ArrayList<String> result = status.getResultAsList("KEY");
//            String dbEncodedString = result.get(0);
//
//            // Decode the return Base64 String to byte array
//            byte[] entry = Base64.getDecoder().decode(dbEncodedString);
//            //byte[] entry = encodedEntry.getBytes();
//
//            System.out.println(entry.toString());
////            final byte[] entry = null;
//
//            // Convert Byte Array to StaticBuffer.
//            return getBuffer(entry);
//           // System.out.println(status.getStatus());
//
//           /* if (status.getStatus()) {
////                final byte[] entry = status.getResult();
//                String encodedEntry =  status.getResult().toString();
//                System.out.println(encodedEntry);
//                System.out.println(encodedEntry);
//                byte[] entry = Base64.getDecoder().decode(encodedEntry);
//                //byte[] entry = encodedEntry.getBytes();
//                System.out.println(entry.toString());
////                final byte[] entry = null;
//                return getBuffer(entry);
//            } else {
//            	System.out.println(" Return null");
//                return null;
//            }*/
//        } catch (Exception e) {
//        	e.printStackTrace();
//            throw new PermanentBackendException(e);
//        }
//        ArrayList arr = new ArrayList<Byte>(10);
//        return StaticArrayBuffer(arr, 0, 0);
    }

    public StaticBuffer get(StaticBuffer key, StoreTransaction txh, String columnName) throws BackendException {

        if (SnowFlakeStoreManager.SKIP_LOG_TABLES) {
            if (GraphUtils.TABLES_TO_OPERATE_ON.contains(db.getTableName())) {
                try {
                    if (isLogged) {
                        log.info(String.format("db=%s, op=get, tx=%s, key=%s", name, txh, key.toString()));
                    }

                    // Convert StaticBuffer to byte array
                    byte[] keyByte = key.as(ENTRY_FACTORY);

                    // Encode byte array to Base64.
//            String encodedkey = Base64.getEncoder().encodeToString(keyByte);

                    // Do DB Operation using Base64 Key.
                    OperationStatus status = db.get(keyByte);
                    if (status.getStatus()) {

                        ArrayList<String> result = status.getResultAsList(columnName);

                        try {
                            String dbEncodedString = result.get(0);
                            // Decode the return Base64 String to byte array
                            byte[] entry = Base64.getDecoder().decode(dbEncodedString);
                            // Convert Byte Array to StaticBuffer.
                            return getBuffer(entry);
                        } catch (IndexOutOfBoundsException e) {
                            // When the DB doesn't contain the required key
                            return null;
                        }
                    }
                    else
                        return null;

                } catch (Exception e) {
                    e.printStackTrace();
                    throw new PermanentBackendException(e);
                }
            }
            else
                return null;
        }
        else {
            try {
                if (isLogged) {
                    log.info(String.format("db=%s, op=get, tx=%s, key=%s", name, txh, key.toString()));
                }

                // Convert StaticBuffer to byte array
                byte[] keyByte = key.as(ENTRY_FACTORY);

                // Encode byte array to Base64.
//            String encodedkey = Base64.getEncoder().encodeToString(keyByte);

                // Do DB Operation using Base64 Key.
                OperationStatus status = db.get(keyByte);
                if (status.getStatus()) {

                    ArrayList<String> result = status.getResultAsList(columnName);

                    try {
                        String dbEncodedString = result.get(0);
                        // Decode the return Base64 String to byte array
                        byte[] entry = Base64.getDecoder().decode(dbEncodedString);
                        // Convert Byte Array to StaticBuffer.
                        return getBuffer(entry);
                    } catch (IndexOutOfBoundsException e) {
                        // When the DB doesn't contain the required key
                        return null;
                    }
                }
                else
                    return null;

            } catch (Exception e) {
                e.printStackTrace();
                throw new PermanentBackendException(e);
            }
        }
    }

    @Override
    public boolean containsKey(StaticBuffer key, StoreTransaction txh) throws BackendException {
        return get(key,txh)!=null;
    }

    @Override
    public void acquireLock(StaticBuffer key, StaticBuffer expectedValue, StoreTransaction txh) throws BackendException {
        if (this.storeManager.getTransaction() == null) {
            if (isLogged) {
                log.warning("Attempt to acquire lock with transactions disabled");
            }
        }
        //else we need no locking
    }

    @Override
    public synchronized void close() throws BackendException {
        try {
            System.out.println("Closing Store DB (DID NOTHING) " + getName());
//            if(isOpen) db.close();
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
        if (isOpen) storeManager.removeDatabase(this);
        isOpen = false;
    }

    private static StaticBuffer getBuffer(byte[] entry) {
        return new StaticArrayBuffer(entry);
    }

//    private static StaticBuffer getBuffer(DatabaseEntry entry) {
//        return new StaticArrayBuffer(entry.getData(),entry.getOffset(),entry.getOffset()+entry.getSize());
//    }
}
