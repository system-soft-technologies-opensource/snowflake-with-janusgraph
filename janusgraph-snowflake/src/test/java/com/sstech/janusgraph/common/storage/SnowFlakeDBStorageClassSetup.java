package com.sstech.janusgraph.common.storage;

import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeKeyValueStore;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.configuration.Configuration;
import org.janusgraph.diskstorage.util.StandardBaseTransactionConfig;
import org.janusgraph.diskstorage.util.time.TimestampProviders;

public class SnowFlakeDBStorageClassSetup {

    // SnowFlake connection parameters
    private String tableName;
//    private String clusterFilePath = "D:\\Projects\\Projects\\Freelancing\\Elysium Analytics\\snowflake\\janus-snowflake\\janusgraph-snowflake\\src\\main\\resources\\credentials\\sf-cluster-debasish.properties";
    private String clusterFilePath = SnowFlakeDBStorageSetup.getClusterFilePath();
    private String graphName = "GRAPHDBTEST";
    private SnowFlakeStoreManager sfManager;
    private SnowFlakeTransaction sfTx;
    private SnowFlakeKeyValueStore sfStore;
    private SnowFlakeDatabase sfDB;

    public SnowFlakeDBStorageClassSetup(String tableName) {
        this.tableName = tableName;
    }

    public void initializeSFEngine() throws Exception {

        initializeSFStoreManager();
        initializeSFTransaction();
        initializeSFDatabase();
        initializeSFStore();
    }

    public void closeSFResources() throws PermanentBackendException {
        try {
            sfStore.close();
            sfDB.close();
            sfTx.commit();
            sfManager.close();
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to close DB or store instance");
        }
    }

    private void initializeSFStoreManager() throws PermanentBackendException {
        try {
            sfManager = new SnowFlakeStoreManager(getSFGraphConfiguration());
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to open connection to BerkleyDB");
        }
    }

    public static BaseTransactionConfig getTxConfig() {
        return StandardBaseTransactionConfig.of(TimestampProviders.MICRO);
    }

    private void initializeSFTransaction() throws BackendException { sfTx = sfManager.getTransaction(); }

    private void initializeSFDatabase() throws Exception { sfDB = new SnowFlakeDatabase(tableName, sfTx); }

    private void initializeSFStore() {
        sfStore = new SnowFlakeKeyValueStore(sfDB, sfManager);
    }

    private Configuration getSFGraphConfiguration() {
        return SnowFlakeDBStorageSetup.getSnowFlakeDBConfiguration();
    }

    public SnowFlakeStoreManager getManager() {
        return sfManager;
    }

    public SnowFlakeTransaction getTransaction() {
        return sfTx;
    }

    public SnowFlakeKeyValueStore getStore() {
        return sfStore;
    }

    public SnowFlakeDatabase getDB() {
        return sfDB;
    }
}
