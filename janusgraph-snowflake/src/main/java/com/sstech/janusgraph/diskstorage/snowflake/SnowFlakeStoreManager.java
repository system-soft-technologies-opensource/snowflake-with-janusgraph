// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



package com.sstech.janusgraph.diskstorage.snowflake;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.common.utils.SingleFileLogger;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnectionManager;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.common.AbstractStoreManager;
import org.janusgraph.diskstorage.configuration.Configuration;
import org.janusgraph.diskstorage.keycolumnvalue.KeyRange;
import org.janusgraph.diskstorage.keycolumnvalue.StandardStoreFeatures;
import org.janusgraph.diskstorage.keycolumnvalue.StoreFeatures;
import org.janusgraph.diskstorage.keycolumnvalue.StoreTransaction;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KVMutation;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.OrderedKeyValueStoreManager;
import org.janusgraph.diskstorage.util.StandardBaseTransactionConfig;
import org.janusgraph.diskstorage.util.time.TimestampProviders;
import org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.*;


public class SnowFlakeStoreManager extends AbstractStoreManager implements OrderedKeyValueStoreManager {

//    private static final Logger log = LoggerFactory.getLogger(SnowFlakeStoreManager.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeStoreManager.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
    private static SingleFileLogger logger = SingleFileLogger.getInstance();
    private Logger log = logger.getLogger();
    private boolean isLogged = true;
    boolean exists;

    static boolean SKIP_LOG_TABLES = true;

    private String RUNNER_NAME = String.format("subThread-[%s-%d-%s]", Thread.currentThread().getName(), Thread.currentThread().getId(), getClass().getSimpleName());

    //Review what could be put here...
//    public static final ConfigNamespace SNOWFLAKE_NS =
//            new ConfigNamespace(GraphDatabaseConfiguration.STORAGE_NS, "sf", "SnowflakeDB JE configuration options");
    private SnowFlakeConnectionManager connectionManager;
    private SnowFlakeConnection connection;

    private Map<String, SnowFlakeKeyValueStore> stores;
    private SnowFlakeTransaction tx;
    private Map<String, SnowFlakeDatabase> databases = new HashMap<>();

    private String dbName;
    private String schemaName;

    private static String dbKey = SnowFlakeDBConfigOptions.GRAPH.getName();
    private static String schemaKey = SnowFlakeDBConfigOptions.SCHEMA.getName();

    private final ExecutorService executorService;
    private Properties connProperties;

    //  protected Environment environment;
    protected StoreFeatures features;
    BaseTransactionConfig cfg = StandardBaseTransactionConfig.of(TimestampProviders.MICRO);

    public SnowFlakeStoreManager(Configuration configuration) throws BackendException {
        super(configuration);
        exists = true;

//        try {
//            log = logger.getLogger();
//        } catch (IOException e) {
//            System.out.println("Unable to create logger class. Not going to use logger and will do sysout");
//            isLogged = false;
//        }
//        log = logger.getLogger();

//        String clusterFilePath = configuration.get(CLUSTER_FILE_PATH);
//        String graphName = configuration.get(GRAPH);
        // TODO: Also read in Graph name & specify it to be appended to properties file
        // TODO: Override SnowFlakeConnectionManager with Properties object as input
        // todo: Done
        Properties connectionProperties = setupConnectionProperties(configuration);
        connProperties = connectionProperties;

        connectionManager = new SnowFlakeConnectionManager(connectionProperties);

        try {
            connectionManager.connect();
            connection = connectionManager.getConnection();
        } catch (SnowFlakeConnectionException e) {
            e.printStackTrace();
            throw new PermanentBackendException("Unable to connect to SnowFlake DB");
        }

        // Create SnowFlake connection object
        stores = new HashMap<>();

        this.executorService = new ThreadPoolExecutor(2,
                5,
                1,
                TimeUnit.MINUTES,
                new LinkedBlockingQueue<>(),
                new ThreadFactoryBuilder()
                        .setDaemon(true)
                        .setNameFormat("SnowFlakeStoreManager[%02d]")
                        .build());

        features = new StandardStoreFeatures.Builder()
                .orderedScan(true)
                //.transactional(transactional)
                .keyConsistent(GraphDatabaseConfiguration.buildGraphConfiguration())
                .locking(true)
                .keyOrdered(true)
                .supportsInterruption(false)
                .optimisticLocking(false)
                .build();
        //  .scanTxConfig(GraphDatabaseConfiguration.buildGraphConfiguration()
        //     .set(ISOLATION_LEVEL, IsolationLevel.READ_UNCOMMITTED.toString()))
            //.supportsInterruption(false)
               // .optimisticLocking(false)
                //.build();

//        features = new StoreFeatures();
//        features.supportsOrderedScan = true;
//        features.supportsUnorderedScan = false;
//        features.supportsBatchMutation = false;
//        features.supportsTxIsolation = transactional;
//        features.supportsConsistentKeyOperations = true;
//        features.supportsLocking = true;
//        features.isKeyOrdered = true;
//        features.isDistributed = false;
//        features.hasLocalKeyPartition = false;
//        features.supportsMultiQuery = false;
    }

    /**review this
     private void initialize(int cachePercent) throws BackendException {
     try {
     EnvironmentConfig envConfig = new EnvironmentConfig();
     envConfig.setAllowCreate(true);
     envConfig.setTransactional(transactional);
     envConfig.setCachePercent(cachePercent);

     if (batchLoading) {
     envConfig.setConfigParam(EnvironmentConfig.ENV_RUN_CHECKPOINTER, "false");
     envConfig.setConfigParam(EnvironmentConfig.ENV_RUN_CLEANER, "false");
     }

     //Open the environment
     environment = new Environment(directory, envConfig);


     } catch (DatabaseException e) {
     throw new PermanentBackendException("Error during Snowflake initialization: ", e);
     }

     }**/

    private void initializeDatabase() throws Exception {
        SnowFlakeDatabase db = new SnowFlakeDatabase("", this.getTransaction());
        OperationStatus result = db.getTables(dbName, schemaName);
        List<String> tables = (List<String>) result.getResult();

        for (String tblName :tables) {
            SnowFlakeDatabase sdb = new SnowFlakeDatabase(tblName, getTransaction());
            sdb.close();
        }
        db.close();
    }

    private Properties setupConnectionProperties(Configuration graphConfiguration) throws BackendException {
        String clusterFilePath = graphConfiguration.get(CLUSTER_FILE_PATH);
        String graphName = graphConfiguration.get(GRAPH);
        String schema = graphConfiguration.get(SCHEMA);

        Properties connectionProperties;

        try {
            InputStream inputHandler = new FileInputStream(clusterFilePath);
            connectionProperties = new Properties();

            connectionProperties.load(inputHandler);

        } catch (IOException e) {
            e.printStackTrace();
            throw new PermanentBackendException(LogUtils.stringFormatter("Unable to locate Cluster Connection File with SnowFlake credentials (%s)", clusterFilePath));
        }

        // If DB & Schema are already specified in Cluster file, then we use that, else we use the one which
        // is part of default Config Builder

        if (!connectionProperties.containsKey(dbKey))
            connectionProperties.put(dbKey, graphName);
        if (!connectionProperties.containsKey(schemaKey))
            connectionProperties.put(schemaKey, schema);

        this.dbName = (String) connectionProperties.get(dbKey);
        this.schemaName = (String) connectionProperties.get(schemaKey);

//        System.out.println("Properties file is: ");
//        connectionProperties.list(System.out);

        return connectionProperties;
    }

    @Override
    public StoreFeatures getFeatures() {
        return features;
    }

    @Override
    public List<KeyRange> getLocalKeyPartition() throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public SnowFlakeTransaction beginTransaction(final BaseTransactionConfig txCfg) throws BackendException {
        if (tx == null) {
            tx = new SnowFlakeTransaction(txCfg, this, connection);
            if (isLogged) {
                log.info(LogUtils.stringFormatter("SnowFlakeTransaction tx created %s", tx.toString()));
            }
        }

        return tx;
    }

    @Override
    public SnowFlakeKeyValueStore openDatabase(String name) throws BackendException {
        Preconditions.checkNotNull(name);
        if (stores.containsKey(name)) {
            return stores.get(name);
        }
        else {
            try {
                SnowFlakeDatabase db = new SnowFlakeDatabase(name, connection);

                if (isLogged)
                    log.info(LogUtils.stringFormatter("Opened database %s", name));

                SnowFlakeKeyValueStore store = new SnowFlakeKeyValueStore(db, this);

                if (isLogged)
                    log.info(LogUtils.stringFormatter("Opened store for %s", name));

                databases.put(name, db);
                stores.put(name, store);

                return store;

            } catch (Exception e) {
                throw new PermanentBackendException(e);
            }
        }
    }

    @Override
    public void mutateMany(Map<String, KVMutation> mutations, StoreTransaction txh) throws BackendException {

//        System.out.println("=========== Mutate Many Ends ==============");
//        System.out.println("Mutation size " + mutations.size());

        for (Map.Entry<String,KVMutation> mutation : mutations.entrySet()) {
            SnowFlakeKeyValueStore store = openDatabase(mutation.getKey());
            KVMutation mutationValue = mutation.getValue();

            if (!mutationValue.hasAdditions() && !mutationValue.hasDeletions()) {
                log.warning(LogUtils.stringFormatter("Empty mutation set for %s, doing nothing", mutation.getKey()));
            } else {
                log.warning(LogUtils.stringFormatter("Mutating {}", mutation.getKey()));
//                System.out.println(LogUtils.stringFormatter("Mutating {}", mutation.getKey()));
            }

            if (mutationValue.hasDeletions()) {
                for (StaticBuffer del : mutationValue.getDeletions()) {
                    store.delete(del,txh);
                    log.info(LogUtils.stringFormatter("Deletion on %s: %s", mutation.getKey(), del.toString()));
//                    System.out.println(LogUtils.stringFormatter("Deletion on {}: {}", mutation.getKey(), String.valueOf(del.getLong(0))));
                }
            }

            if (mutationValue.hasAdditions()) {
                for (KeyValueEntry entry : mutationValue.getAdditions()) {
                    store.insert(entry.getKey(),entry.getValue(),txh);
                    log.info(LogUtils.stringFormatter("Insertion on %s: %s", mutation.getKey(), entry.toString()));
//                    System.out.println(LogUtils.stringFormatter("Insertion on {}: k={}, v={}",
//                        mutation.getKey(), String.valueOf(entry.getKey().getLong(0)),
//                        String.valueOf(entry.getValue().getLong(0))));
                }
            }
        }
//        System.out.println("=========== Mutate Many Ends ==============");
    }

    void removeDatabase(SnowFlakeKeyValueStore db) {
        if (!stores.containsKey(db.getName())) {
            throw new IllegalArgumentException("Tried to remove an unkown database from the storage manager");
        }
        String name = db.getName();
        stores.remove(name);
        log.info(LogUtils.stringFormatter("Removed database %s", name));
    }

    ExecutorService getExecutorService() {
        return this.executorService;
    }

    @Override
    public void close() {

        databases.forEach((name, database) -> {
            try {
                database.close();
                log.info("Closed " + name);
//                System.out.println("Closed DB " + name + " called but closing it");
//                System.out.println("Closed DB " + name);
                throw new PermanentBackendException("mock exception");
            } catch (PermanentBackendException e) {
                log.severe(LogUtils.stringFormatter("Error closing %s ", name));
            }
        });

        stores.forEach( (name, store) -> {
            try {
                store.close();
//                System.out.println("Removed Store called " + name + " but removing it");
                log.info(LogUtils.stringFormatter("Closed store %s ", name));
                throw new PermanentBackendException("mock exception");
            } catch (BackendException e) {
                log.severe(LogUtils.stringFormatter("Error closing connection to store %s ", name));
            }
        });

        this.tx.close();
//        System.out.println("Closed transaction " + tx.toString());
        this.executorService.shutdownNow();
//
////        if (environment != null) {
//            if (!stores.isEmpty())
//                throw new IllegalStateException("Cannot shutdown manager since some databases are still open with stores " + stores.toString());
//            try {
//                // TODO this looks like a race condition
//                //Wait just a little bit before closing so that independent transaction threads can clean up.
//                Thread.sleep(30);
//            } catch (InterruptedException e) {
//                //Ignore
//            }
//            try{
//                this.close();
//            } catch (BackendException e) {
//                throw new PermanentBackendException("Could not close Snowflake database", e);
//            }

//            try {
//                environment.close();
//            } catch (DatabaseException e) {
//                throw new PermanentBackendException("Could not close Snowflake database", e);
//            }
//        }
    }

//    private static final Transaction NULL_TRANSACTION = null;

    @Override
    public void clearStorage() throws BackendException {

        System.out.println("Clear storage called");

        if (!stores.isEmpty()) {
            throw new IllegalStateException("Cannot delete store, since database is open: " + stores.keySet().toString());
        }
        //list the tables from snowflake db and remove the respective table..
        // - in snowflake db - 2 functions, list table and remove table.
//        SnowFlakeDatabase db = null;
        try{
            SnowFlakeDatabase db = new SnowFlakeDatabase("", this.getTransaction());///change it to transaction as required.

            OperationStatus tableList = db.getTables(dbName, schemaName);

            if (tableList.getStatus()) {
                List<String> tables = (List<String>) tableList.getResult();

                for (final String tblName :tables)
                {
                    OperationStatus status = db.removeTable(schemaName, tblName);
                    if (status.getStatus()) {
                        System.out.println("Removed table (delete) " + tblName);
                        log.info(LogUtils.stringFormatter("Removed database %s (clearStorage)", db.getTableName()));
                        exists = false;
                    }
                    else {
                        log.severe(LogUtils.stringFormatter("Couldn't remove table %s from snowflake backend", tblName));
                    }
                }
            }
            else{
                throw new PermanentBackendException("Unable to fetch table list from snowflake due to error");
            }
        }
        catch (IOException e) {
            log.severe("Couldnt connect to snowflake as log file couldn't be created");
        }
        catch (SnowFlakeConnectionException e) {
            log.severe("Couldnt connect to snowflake");
        }
        close();
        // IOUtils.deleteFromDirectory(directory);
    }

    @Override
    public boolean exists() throws BackendException {
//        return !environment.getDatabaseNames().isEmpty();
        // check table names not empty here..
        return exists;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName() + (connection==null ? "": connection.getURL());
    }

    private static class TransactionBegin extends Exception {
        private static final long serialVersionUID = 1L;
        private TransactionBegin(String msg) {
            super(msg);
        }
    }

    public SnowFlakeTransaction getTransaction() throws BackendException {
        if (tx == null) {
            tx = new SnowFlakeTransaction(cfg, this, connection);

            if (isLogged) {
                log.info(LogUtils.stringFormatter("SnowFlakeTransaction tx created %s", tx.toString()));
            }
        }
        return tx;
    }

    public SnowFlakeTransaction createFreshTransaction() throws PermanentBackendException, SnowFlakeConnectionException {
        SnowFlakeConnectionManager manager = new SnowFlakeConnectionManager(connProperties);
        manager.connect();
        SnowFlakeConnection conn = manager.getConnection();
        SnowFlakeTransaction tx = new SnowFlakeTransaction(StandardBaseTransactionConfig.of(TimestampProviders.MICRO), this, conn);
        return tx;
    }
}
